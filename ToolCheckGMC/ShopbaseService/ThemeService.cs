﻿using ShopifySharp;
using ShopifySharp.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ToolCheckGMC.ServiceShopbase.ResponseEntities;
using ToolCheckGMC.ShopbaseService.ResponseEntities;

namespace ToolCheckGMC.ServiceShopbase
{
    public class ThemeService: ShopBaseService
    {
        public ThemeService(string onShopBaseUrl, string shopAccessToken, string shopApiKey) : base(onShopBaseUrl, shopAccessToken, shopApiKey) { }
        public virtual async Task<RequestResult<List<PageShopbase>>> GetPages(CancellationToken cancellationToken = default)
        {
            var request = PrepareRequest("pages.json");
            return await ExecuteRequestAsync<List<PageShopbase>>(request, HttpMethod.Get, cancellationToken ,rootElement: "pages");
        }
        public virtual async Task<RequestResult<PageShopbase>> GetPage(long pageId, CancellationToken cancellationToken = default)
        {
            var request = PrepareRequest($"pages/{pageId}.json");
            return await ExecuteRequestAsync<PageShopbase>(request, HttpMethod.Get, cancellationToken, null, rootElement: "page");
        }
        public virtual async Task<RequestResult<PageShopbase>> UpdatePage(PageShopbase page, CancellationToken cancellationToken = default)
        {
            var request = PrepareRequest($"pages/{page.Id}.json");
            var content = new JsonContent(new
            {
                page = page
            });
            return await ExecuteRequestAsync<PageShopbase>(request, HttpMethod.Put, cancellationToken, content, rootElement: "page");
        }
        public virtual async Task<RequestResult<PageShopbase>> CreatePage(PageShopbase page, CancellationToken cancellationToken = default)
        {
            var request = PrepareRequest("pages.json");

            var content = new JsonContent(new
            {
                page = page
            });
            return await ExecuteRequestAsync<PageShopbase>(request, HttpMethod.Post,cancellationToken ,content, "page");
        }


       
        public virtual async Task<RequestResult<List<Collection>>> GetAllCollections(CancellationToken cancellationToken = default)
        {
            var request = PrepareRequest("collections.json");
            return await ExecuteRequestAsync<List<Collection>>(request, HttpMethod.Get,cancellationToken ,rootElement: "collections");
        }
        public virtual async Task<RequestResult<Collection>> CreateCollection(Collection collection, CancellationToken cancellationToken = default)
        {
            var request = PrepareRequest("smart_collections.json");
            var content = new JsonContent(new
            {
                smart_collection = collection
            });
            return await ExecuteRequestAsync<Collection>(request, HttpMethod.Post,cancellationToken ,content, "smart_collection");
        }
        public virtual async Task<RequestResult<Collection>> UpdateCollection(Collection collection, CancellationToken cancellationToken = default)
        {
            var request = PrepareRequest($"smart_collections/{collection.Id}.json");
            var content = new JsonContent(new
            {
                smart_collection = collection
            });
            return await ExecuteRequestAsync<Collection>(request, HttpMethod.Put,cancellationToken ,content, "smart_collection");
        }
    }
}
