﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolCheckGMC.ServiceShopbase.ResponseEntities
{
    public class PageShopbase
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("body_html")]
        public string BodyHtml { get; set; }
        [JsonProperty("handle")]
        public string Handle { get; set; }
        [JsonProperty("publish")]
        public bool Publish { get; set; }
    }
}
