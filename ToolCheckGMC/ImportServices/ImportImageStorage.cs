﻿using CloudinaryDotNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPWrapper;

namespace ToolCheckGMC.ImportServices
{
    public static class ImportImageStorage
    {
        public static string ImportImageStorageFolder = ConfigurationManager.AppSettings["ImportImageStorageFolder"].ToString();

        public static ImageStorage RetrieveImageStorage(int id)
        {
            using (bonzaroEntities db = new bonzaroEntities())
            {
                return db.ImageStorages.Find(id);
            }
        }

        public static System.Drawing.Image ReadImage(int id)
        {
            if (string.IsNullOrEmpty(ImportImageStorageFolder))
                throw new Exception("ImportImageStorageFolder not found");

            bonzaroEntities db = new bonzaroEntities();
            var imageStorage = db.ImageStorages.Find(id);
            if (imageStorage == null)
                throw new Exception($"No image storage was found with id {id}");

            if (string.IsNullOrEmpty(imageStorage.Path))
                throw new Exception("Unkown image location");

            var imageFile = Path.Combine(ImportImageStorageFolder, imageStorage.Path);
            if (!File.Exists(imageFile))
                throw new Exception($"Image file is not existed");

            System.Drawing.Image img = null;
            if (!imageFile.EndsWith(".webp"))
                img = System.Drawing.Image.FromFile(imageFile);
            else
            {
                using (WebPWrapper.WebP webp = new WebPWrapper.WebP())
                    img = webp.Load(imageFile);
            }
            return img;
        }
    }
}
