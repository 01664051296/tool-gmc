﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolCheckGMC.ShopbaseService.ResponseEntities
{
    public class TypeOption
    {
        [JsonProperty("subject")]
        public string Subject { get; set; }
        [JsonProperty("subject_id")]
        public string SubjectId { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
    }
}
