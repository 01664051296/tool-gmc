﻿using CsvHelper;
using Google.Apis.Auth.OAuth2;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using ShopifySharp;
using ShopifySharp.Filters;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ToolCheckGMC.ImportServices;
using ToolCheckGMC.Model;
using ToolCheckGMC.ServiceShopbase;
using ToolCheckGMC.ServiceShopbase.ResponseEntities;
using ToolCheckGMC.ShopbaseService.ResponseEntities;
using Product = ShopifySharp.Product;

namespace ToolCheckGMC
{
    public partial class Main : Form
    {
        MerchantCenterEntities db = new MerchantCenterEntities();
        bonzaroEntities dbTomeey = new bonzaroEntities();
        eggfmsEntities dbW9 = new eggfmsEntities();
        List<StoreAuthencation> listStore = new List<StoreAuthencation>();
        int storeSelected = 0;
        List<ShopifyCenterStore> storeTomeeySelected = new List<ShopifyCenterStore>();
        int storeSelectedTab5 = 0;
        int storeSelectedTab6 = 0;
        List<ShopifyCustomService.Product> listProductPrepare = new List<ShopifyCustomService.Product>();
        List<Product> listProductShopifyPrepare = new List<Product>();
        public Main()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            loadDataGridAccountGMC();
            loadDataGridShopbaseStores();
            loadDataListTomeeyStores();
            btnBackupShopify.Hide();
            btnDeleteProductError.Hide();
            btnProductCount.Hide();
            btnExportShopbaseCSV.Hide();
            try
            {
                var stores = db.Shopbase_Stores.OrderBy(p => p.StoreName).ToList();
                foreach (var store in stores)
                {
                    comboBox1.Items.Add(store);
                    comboBox2.Items.Add(store);
                }
                comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
                comboBox1.DisplayMember = "StoreName";
                comboBox1.ValueMember = "Id";
                comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
                comboBox2.DisplayMember = "StoreName";
                comboBox2.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #region tab merchant center
        private void loadDataGridAccountGMC(string keySearch = null)
        {
            //Prepare grid view
            dataGridView1.ColumnCount = 8;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.Columns[0].Name = "Merchant ID";
            dataGridView1.Columns[1].Name = "Store Name";
            dataGridView1.Columns[2].Name = "Note";
            dataGridView1.Columns[3].Name = "Update Time";
            dataGridView1.Columns[4].Name = "Total Active";
            dataGridView1.Columns[5].Name = "Total Pending";
            dataGridView1.Columns[6].Name = "Total Disapproved";
            dataGridView1.Columns[7].Name = "Is Active";
            dataGridView1.Rows.Clear();
            var allAccount = db.Accounts.Where(p => !p.IsDeleted).OrderByDescending(p => p.IsActive).ToList();
            if (!string.IsNullOrEmpty(keySearch))
                allAccount = allAccount.Where(p => p.StoreName.ToUpper().Contains(keySearch)).ToList();
            foreach (var item in allAccount)
            {
                string[] row = new string[] { "", "", "", "", "", "", "" };
                row = new string[] { item.MerchantId.ToString(), item.StoreName, item.Note, item.ModificationTime.HasValue ? item.ModificationTime.Value.ToString("dd/MM/yyyy  HH:mm:ss") : "Not update", item.TotalActive.ToString(), item.TotalPending.ToString(), item.TotalDisapproved.ToString(), item.IsActive ? "Active" : "Disapproved" };
                dataGridView1.Rows.Add(row);
            }
            DataGridViewButtonColumn btnCheckGMC = new DataGridViewButtonColumn();
            dataGridView1.Columns.Add(btnCheckGMC);
            btnCheckGMC.HeaderText = "Click To Check";
            btnCheckGMC.Text = "Check";
            btnCheckGMC.Name = "btnCheckGMC";
            btnCheckGMC.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn btnDelete = new DataGridViewButtonColumn();
            dataGridView1.Columns.Add(btnDelete);
            btnDelete.HeaderText = "Click To Delete";
            btnDelete.Text = "Delete";
            btnDelete.Name = "btnDelete";
            btnDelete.UseColumnTextForButtonValue = true;
        }
        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            loadDataGridAccountGMC(txtSearch.Text.Trim().ToUpper());
        }
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 8)
            {
                var id = Convert.ToInt64(dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString());
                CheckAccountStatus(db.Accounts.FirstOrDefault(p => p.MerchantId == id));
                db.SaveChanges();
                loadDataGridAccountGMC(txtSearch.Text.Trim().ToUpper());
            }
            if (e.ColumnIndex == 9)
            {
                var id = Convert.ToInt64(dataGridView1.Rows[e.RowIndex].Cells[0].FormattedValue.ToString());
                DialogResult dr = MessageBox.Show($"Do you want delete this account \"{id}\"?", "Notification", MessageBoxButtons.YesNo);
                switch (dr)
                {
                    case DialogResult.Yes:
                        db.Accounts.Remove(db.Accounts.FirstOrDefault(p => p.MerchantId == id));
                        db.SaveChanges();
                        break;
                    case DialogResult.No:
                        break;
                }
                loadDataGridAccountGMC(txtSearch.Text.Trim().ToUpper());
            }
        }
        public Account CheckAccountStatus(Account item)
        {

            string GOOGLE_CONTENT_API_SCOPE = "https://www.googleapis.com/auth/content";
            try
            {
                var token = GoogleCredential
                                           .FromJson(item.Json) // Loads key file
                                           .CreateScoped(GOOGLE_CONTENT_API_SCOPE) // Gathers scopes requested
                                           .UnderlyingCredential // Gets the credentials
                                           .GetAccessTokenForRequestAsync().Result; // Gets the Access Token

                RestClient client = new RestClient($"https://shoppingcontent.googleapis.com/content/v2.1/{item.MerchantId}/accountstatuses/{item.MerchantId}");
                RestRequest getRequest = new RestRequest(Method.GET);
                getRequest.AddHeader("Accept", "application/json");
                getRequest.AddHeader("authorization", "Bearer " + token);

                IRestResponse getResponse = client.Execute(getRequest);

                var statusResp = JsonConvert.DeserializeObject<GMCAccountStatusResp>(getResponse.Content);
                item.TotalActive = 0;
                item.TotalPending = 0;
                item.TotalDisapproved = 0;
                item.TotalExpiring = 0;
                //prepare content response
                string content = $"***** \"{item.StoreName}\": google merchant statistics today! ***** \n";
                content += statusResp.WebsiteClaimed ? "- Verified website." : "- The site has not been authenticated.";
                foreach (var productFeed in statusResp.Products)
                {
                    item.TotalActive += productFeed.Statistics.Active;
                    item.TotalPending += productFeed.Statistics.Pending;
                    item.TotalDisapproved += productFeed.Statistics.Disapproved;
                    item.TotalExpiring += productFeed.Statistics.Expiring;
                    item.IsActive = item.TotalActive > 0 || item.TotalPending > 0;
                    content += $"\n- Country :{productFeed.Country}. \nThere are: \n\t{item.TotalActive} active products. \n\t{item.TotalPending} pending products. \n\t{item.TotalExpiring} expiring products. \n\t{item.TotalDisapproved} disapproved products. \n";
                    content += productFeed.ItemLevelIssues.Any() ? "- Product issues:\n" : "- No problem about the product.\n";
                    int totalIssues = 1;
                    foreach (var itemLevelIssues in productFeed.ItemLevelIssues)
                    {
                        content += $"* Issue {totalIssues}: {itemLevelIssues.Description}.\n\t- Servability: {itemLevelIssues.Servability}. \n\t- Number of products affected {itemLevelIssues.NumItems}. \n\t- Detail: {itemLevelIssues.Detail}.\n\t- Documentation: {itemLevelIssues.Documentation} \n";
                        totalIssues++;
                    }
                }
                item.ModificationTime = DateTime.Now;


                rtbResponse.Text = content;
                rtbResponse.SelectionStart = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Response", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return item;
        }
        private void btnAddGMC_Click(object sender, EventArgs e)
        {
            AddNewGMC popup = new AddNewGMC();
            DialogResult dialogresult = popup.ShowDialog();
            loadDataGridAccountGMC(txtSearch.Text.Trim().ToUpper());
            popup.Dispose();
        }
        #endregion
        #region tab shopbase
        private void loadDataListTomeeyStores()
        {
            foreach (var store in dbTomeey.ShopifyCenterStores.ToList())
            {
                if (string.IsNullOrEmpty(store.Name))
                    store.Name = "None";
                store.Name = store.Id.ToString() + " - " + store.Name;
                checkedListStore.Items.Add(store);
            }
            checkedListStore.DisplayMember = "Name";
            checkedListStore.ValueMember = "Id";
        }
        private void loadDataGridShopbaseStores()
        {
            //Prepare grid view
            dataGridView2.ColumnCount = 6;
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView2.Columns[0].Name = "ID";
            dataGridView2.Columns[1].Name = "Merchant ID";
            dataGridView2.Columns[2].Name = "Store Name";
            dataGridView2.Columns[3].Name = "Api Key";
            dataGridView2.Columns[4].Name = "Email Support";
            dataGridView2.Columns[5].Name = "Type Project";
            dataGridView2.Rows.Clear();
            var allStore = db.Shopbase_Stores.ToList();
            foreach (var item in allStore)
            {
                string[] row = new string[] { "", "", "", "", "", "", "" };
                row = new string[] { item.Id.ToString(), item.AccountGMCId.HasValue ? item.AccountGMCId.Value.ToString() : "Pending", item.StoreName, item.ApiKey, item.EmailSupport, item.TypeProject.ToString() };
                dataGridView2.Rows.Add(row);
            }
            DataGridViewButtonColumn btnUpdateStore = new DataGridViewButtonColumn();
            dataGridView2.Columns.Add(btnUpdateStore);
            btnUpdateStore.HeaderText = "Update";
            btnUpdateStore.Text = "Update";
            btnUpdateStore.Name = "btnUpdateShopbase";
            btnUpdateStore.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn btnEdit = new DataGridViewButtonColumn();
            dataGridView2.Columns.Add(btnEdit);
            btnEdit.HeaderText = "Edit";
            btnEdit.Text = "Edit";
            btnEdit.Name = "btnEdit";
            btnEdit.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn btnDelete = new DataGridViewButtonColumn();
            dataGridView2.Columns.Add(btnDelete);
            btnDelete.HeaderText = "Delete";
            btnDelete.Text = "Delete";
            btnDelete.Name = "btnDelete";
            btnDelete.UseColumnTextForButtonValue = true;
        }
        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                var id = int.Parse(dataGridView2.Rows[e.RowIndex].Cells[0].FormattedValue.ToString());
                var store = db.Shopbase_Stores.FirstOrDefault(p => p.Id == id);
                if (store.AdminUrl.Contains("myshopify"))
                    UpdateShopifyStore(store);
                else
                    UpdateShopbaseStore(store);
            }
            if (e.ColumnIndex == 8)
            {
                var id = int.Parse(dataGridView2.Rows[e.RowIndex].Cells[0].FormattedValue.ToString());
                db.Shopbase_Stores.Remove(db.Shopbase_Stores.FirstOrDefault(p => p.Id == id));
                db.SaveChanges();
                loadDataGridShopbaseStores();
            }
        }
        private async void UpdateShopbaseStore(Shopbase_Store store)
        {
            try
            {
                var _themeShopbase = new ServiceShopbase.ThemeService(store.AdminUrl, store.Password, store.ApiKey);
                if (checkBoxUpdatePage.Checked)
                {
                    //Update Pages
                    var allPage = await _themeShopbase.GetPages();
                    var allPageUpdate = db.LegalPages.Where(p => p.TypeProject == store.TypeProject).ToList();
                    foreach (var page in allPageUpdate)
                    {
                        string htmlBody = PrepareHtmlPage(store, page.Template);
                        var pageAvailable = allPage.Result.FirstOrDefault(p => p.Title.Contains(page.NamePage));
                        if (pageAvailable != null)
                        {
                            pageAvailable.BodyHtml = htmlBody;
                            pageAvailable.Publish = true;
                            await _themeShopbase.UpdatePage(pageAvailable);
                        }
                        else
                        {
                            var newPage = new PageShopbase
                            {
                                Title = page.NamePage,
                                BodyHtml = htmlBody,
                                Publish = true
                            };
                            await _themeShopbase.CreatePage(newPage);
                        }
                    }
                }
                if (checkBoxUpdateCollections.Checked)
                {
                    //var menus = await _themeShopbase.GetNavigationMenus();
                    var collections = await _themeShopbase.GetAllCollections();
                    var collectionsToClone = await GetCollectionsBySotreId(4199);
                    foreach (var collection in collectionsToClone)
                    {
                        var collectionAvailable = collections.Result.FirstOrDefault(p => p.Title.Contains(collection.Title));
                        if (collectionAvailable == null)
                        {
                            await _themeShopbase.CreateCollection(collection);
                        }
                        else
                        {
                            collectionAvailable.Disjunctive = collection.Disjunctive;
                            collectionAvailable.Rules = collection.Rules;
                            await _themeShopbase.UpdateCollection(collectionAvailable);
                        }
                    }
                }
                if (checkBoxUpdateProducts.Checked)
                {
                    var productService = new ShopbaseService.ProductService(store.AdminUrl, store.Password, store.ApiKey);
                    productService.SetExecutionPolicy(new SmartRetryExecutionPolicy());
                    List<long> ids = new List<long>();
                    var filter = new ProductListFilter()
                    {
                        //Title = txtTitleFilter.Text,
                    };
                    var allProduct = await productService.ListAsync(filter);
                    while (true)
                    {
                        foreach (var product in allProduct.Items)
                        {
                            if (!product.Images.Any())
                            {
                                await productService.DeleteAsync(product.Id.Value);
                            }
                            product.Title = FixTitle(product.Title);
                            await productService.UpdateAsync(product.Id.Value, product);
                        }
                        if (!allProduct.HasNextPage)
                        {
                            break;
                        }
                        allProduct = await productService.ListAsync(allProduct.GetNextPageFilter());
                    }
                }
                MessageBox.Show("Store updated successfully", "Infomation!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private static string FixTitle(string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;
            source = source.ToLower();
            var words = source.Split(' ');
            words = words.Where(p => !string.IsNullOrEmpty(p)).ToArray();
            for (int i = 0; i < words.Count(); i++)
            {
                words[i] = ToUpperFirstLetter(words[i].ToLower());
            }
            return String.Join(" ", words.ToArray());
        }
        private static string RemoveLastWord(string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;
            var words = source.Split(' ');
            words = words.Where(p => !string.IsNullOrEmpty(p)).ToArray();
            for (int i = 0; i < words.Count(); i++)
            {
                if (i == words.Count() - 1)
                    words[i] = "";
            }
            return String.Join(" ", words.ToArray());
        }
        private static string ToUpperFirstLetter(string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;
            // convert to char array of the string
            char[] letters = source.ToCharArray();
            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);
            // return the array made of the new char array
            return new string(letters);
        }
        private async void UpdateShopifyStore(Shopbase_Store store)
        {
            try
            {
                if (checkBoxUpdatePage.Checked)
                {
                    var _pageService = new ShopifySharp.PageService(store.AdminUrl, store.Password);
                    //Update Pages
                    var allPage = await _pageService.ListAsync();
                    var allPageUpdate = db.LegalPages.Where(p => p.TypeProject == store.TypeProject).ToList();
                    foreach (var page in allPageUpdate)
                    {
                        string htmlBody = PrepareHtmlPage(store, page.Template);
                        var pageAvailable = allPage.Items.FirstOrDefault(p => p.Title.Contains(page.NamePage));
                        if (pageAvailable != null)
                        {
                            pageAvailable.BodyHtml = htmlBody;
                            await _pageService.UpdateAsync(pageAvailable.Id.Value, pageAvailable);
                        }
                        else
                        {
                            var newPage = new Page
                            {
                                Title = page.NamePage,
                                BodyHtml = htmlBody
                            };
                            await _pageService.CreateAsync(newPage);
                        }
                    }
                }
                if (checkBoxUpdateProducts.Checked)
                {
                    var productService = new ShopifySharp.ProductService(store.AdminUrl, store.Password);
                    productService.SetExecutionPolicy(new SmartRetryExecutionPolicy());
                    var allProduct = await productService.ListAsync();
                    foreach (var product in allProduct.Items)
                    {
                        foreach (var variant in product.Variants)
                        {
                            variant.InventoryQuantity = null;
                            variant.CompareAtPrice = null;
                            variant.InventoryManagement = "";
                            variant.FulfillmentService = "Manual";
                            variant.InventoryItemId = null;
                        }
                        Thread.Sleep(500);
                        await productService.UpdateAsync(product.Id.Value, product);
                    }
                    var inventoryService = new ShopifySharp.InventoryLevelService(store.AdminUrl, store.Password);
                    inventoryService.SetExecutionPolicy(new SmartRetryExecutionPolicy());
                }

                MessageBox.Show("Store updated successfully", "Infomation!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        protected async Task<List<Collection>> GetCollectionsBySotreId(int id)
        {
            var allColections = new List<Collection>();
            try
            {
                var store = db.Shopbase_Stores.FirstOrDefault(p => p.Id == id);
                var _themeShopbase = new ServiceShopbase.ThemeService(store.AdminUrl, store.Password, store.ApiKey);
                allColections = (await _themeShopbase.GetAllCollections()).Result;
                return allColections.Where(p => p.Type == "smart").ToList();
            }
            catch
            {
                //do not thing
            }
            return allColections;
        }
        protected string PrepareHtmlPage(Shopbase_Store store, string fullTemplate)
        {
            string htmlBody = fullTemplate;
            try
            {
                htmlBody = htmlBody.Replace("%name%", store.StoreName);
                htmlBody = htmlBody.Replace("%email%", store.EmailSupport);
                htmlBody = htmlBody.Replace("%address%", store.Address);
                htmlBody = htmlBody.Replace("%phone%", store.PhoneNumber);
                return htmlBody;
            }
            catch { }
            return null;
        }
        private void btnAddStore_Click(object sender, EventArgs e)
        {
            AddNewStore popup = new AddNewStore();
            DialogResult dialogresult = popup.ShowDialog();
            loadDataGridShopbaseStores();
            popup.Dispose();
        }



        #endregion
        #region Shopify
        private void loadDataGridShopifyStores()
        {
            //Prepare grid view
            dataGridView2.ColumnCount = 6;
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView2.Columns[0].Name = "ID";
            dataGridView2.Columns[1].Name = "Merchant ID";
            dataGridView2.Columns[2].Name = "Store Name";
            dataGridView2.Columns[3].Name = "Api Key";
            dataGridView2.Columns[4].Name = "Email Support";
            dataGridView2.Columns[5].Name = "Type Project";
            dataGridView2.Rows.Clear();
            var allStore = db.Shopbase_Stores.ToList();
            foreach (var item in allStore)
            {
                string[] row = new string[] { "", "", "", "", "", "", "" };
                row = new string[] { item.Id.ToString(), item.AccountGMCId.HasValue ? item.AccountGMCId.Value.ToString() : "Pending", item.StoreName, item.ApiKey, item.EmailSupport, item.TypeProject.ToString() };
                dataGridView2.Rows.Add(row);
            }
            DataGridViewButtonColumn btnUpdateStore = new DataGridViewButtonColumn();
            dataGridView2.Columns.Add(btnUpdateStore);
            btnUpdateStore.HeaderText = "Update To Shopbase";
            btnUpdateStore.Text = "Update To Shopbase";
            btnUpdateStore.Name = "btnUpdateShopbase";
            btnUpdateStore.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn btnEdit = new DataGridViewButtonColumn();
            dataGridView2.Columns.Add(btnEdit);
            btnEdit.HeaderText = "Edit";
            btnEdit.Text = "Edit";
            btnEdit.Name = "btnEdit";
            btnEdit.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn btnDelete = new DataGridViewButtonColumn();
            dataGridView2.Columns.Add(btnDelete);
            btnDelete.HeaderText = "Delete";
            btnDelete.Text = "Delete";
            btnDelete.Name = "btnDelete";
            btnDelete.UseColumnTextForButtonValue = true;
        }
        #endregion

        private void btnAddStoreShopify_Click(object sender, EventArgs e)
        {

        }

        private async void btnClone_Click(object sender, EventArgs e)
        {
            var myShopifyUrl = "https://rockrolls-store.myshopify.com/admin/";
            var shopAccessToken = "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";
            //var myShopifyUrl = "https://batenstore.myshopify.com/admin";
            //var shopAccessToken = "d25d7ef780ffdb87b35fdb274e7f6355";
            //var myShopifyUrl = "https://stocks-repository.myshopify.com/";
            //var shopAccessToken = "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";
            //var myShopifyUrl = "https://face-mask-tm.myshopify.com/";
            //var shopAccessToken = "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";
            var productService = new ShopifySharp.ProductService(myShopifyUrl, shopAccessToken);
            productService.SetExecutionPolicy(new SmartRetryExecutionPolicy());
            var serviceProductVariant = new ProductVariantService(myShopifyUrl, shopAccessToken);
            serviceProductVariant.SetExecutionPolicy(new SmartRetryExecutionPolicy());
            //bool scaning = true;
            var filter = new ProductListFilter()
            {
                Limit = 50,
                Title = txtTitleFilter.Text
                //PublishedStatus = "published",
                //PublishedAtMin = DateTime.Now.AddDays(-5)
                //ProductType = "Face Protection",
                //Vendor = "Lumbini Graphics"
            };
            var allProduct = await productService.ListAsync(filter);
            int productCount = 0;
            int totalSuccess = 0;
            int totalUpdated = 0;
            long maxProductIdUpdated = Common.LoadLogMaxProductIdDreamshipShopify();
            while (true)
            {
                //Update 
                var allProductItems = allProduct.Items;
                //totalSuccess += allProduct.Items.Count();
                foreach (var product in allProductItems)
                {
                    try
                    {
                        if (!product.Images.Any())
                        {
                            await productService.DeleteAsync(product.Id.Value);
                            continue;
                        }
                        #region UpdateQuantity
                        if (product.Variants.Any(p => p.InventoryQuantity.HasValue && p.InventoryQuantity.Value < 10 || !p.InventoryQuantity.HasValue))
                        {
                            foreach (var variant in product.Variants)
                            {
                                variant.FulfillmentService = "Manual";
                                variant.InventoryQuantity = 999999;
                            }
                            await productService.UpdateAsync(product.Id.Value, product);
                        }
                        #endregion
                        #region updateSock
                        //if (!product.Vendor.Contains("Nice Socks"))
                        //{
                        //    product.Vendor = "Nice Socks 2";
                        //    product.ProductType = "Sock";
                        //    var productUpdated = await productService.UpdateAsync(product.Id.Value, product);
                        //    totalUpdated++;
                        //    rtbLogTab3.Focus();
                        //    rtbLogTab3.AppendText(Environment.NewLine);
                        //    rtbLogTab3.AppendText($"- Updated: '{product.Title}' --- {totalUpdated}/{totalSuccess} scanned product");
                        //}
                        #endregion
                        #region updateFaceMask
                        //totalUpdated++;
                        //try
                        //{
                        //    if (product.Variants.Any(p => !string.IsNullOrEmpty(p.Option1) && p.Option1.ToUpper().Contains("PACK")
                        //    || !string.IsNullOrEmpty(p.Option2) && p.Option2.ToUpper().Contains("PACK")
                        //    || !string.IsNullOrEmpty(p.Option3) && p.Option3.ToUpper().Contains("PACK")))
                        //        continue;
                        //    List<ProductOption> productOptions = new List<ProductOption>();
                        //    //Values
                        //    var valuesPack = new List<string>();
                        //    valuesPack.Add("One Item");
                        //    valuesPack.Add("Pack 3");
                        //    valuesPack.Add("Pack 5");
                        //    valuesPack.Add("Pack 10");
                        //    var option = new ProductOption();
                        //    option.Name = "Pack";
                        //    option.Values = valuesPack;
                        //    productOptions.Add(option);
                        //    var allVariant = new List<ProductVariant>();
                        //    var variants = new List<ProductVariant>();
                        //    var firstVariant = product.Variants.FirstOrDefault();
                        //    foreach (var quantity in valuesPack)
                        //    {
                        //        var newVariant = new ProductVariant();
                        //        switch (quantity)
                        //        {
                        //            case "One Item":
                        //                newVariant.Price = 14.99m;
                        //                break;
                        //            case "Pack 3":
                        //                newVariant.Price = 39.99m;
                        //                break;
                        //            case "Pack 5":
                        //                newVariant.Price = 59.99m;
                        //                break;
                        //            case "Pack 10":
                        //                newVariant.Price = 104.99m;
                        //                break;
                        //            default: break;
                        //        }
                        //        newVariant.Option1 = quantity;
                        //        newVariant.Option2 = null;
                        //        newVariant.Option3 = null;
                        //        newVariant.Position = firstVariant.Position;
                        //        newVariant.Barcode = firstVariant.Barcode;
                        //        newVariant.SKU = firstVariant.SKU + "-" + quantity;
                        //        newVariant.InventoryQuantity = 9999999;
                        //        newVariant.FulfillmentService = "Manual";
                        //        newVariant.Grams = firstVariant.Grams;
                        //        newVariant.ImageId = firstVariant.ImageId;
                        //        newVariant.InventoryPolicy = firstVariant.InventoryPolicy;
                        //        newVariant.CompareAtPrice = firstVariant.CompareAtPrice;
                        //        newVariant.Title = firstVariant.Title;
                        //        newVariant.Weight = firstVariant.Weight;
                        //        newVariant.WeightUnit = firstVariant.WeightUnit;
                        //        allVariant.Add(newVariant);
                        //    }
                        //    product.Options = productOptions;
                        //    product.Variants = allVariant;
                        //    var productUpdated = await productService.UpdateAsync(product.Id.Value, product);
                        //    totalSuccess++;
                        //    rtbLogTab3.Focus();
                        //    rtbLogTab3.AppendText(Environment.NewLine);
                        //    rtbLogTab3.AppendText($"- Updated: '{product.Title}' --- {totalSuccess}/{totalUpdated} scanned product");
                        //}
                        //catch (Exception ex)
                        //{
                        //    rtbLogTab3.Focus();
                        //    rtbLogTab3.AppendText(Environment.NewLine);
                        //    rtbLogTab3.AppendText($"- Error: '{product.Title}' --- {totalSuccess}/{totalUpdated} scanned product");
                        //    rtbLogTab3.AppendText(Environment.NewLine);
                        //    rtbLogTab3.AppendText($"-> Detail: '{ex.Message}'");
                        //}
                        #endregion
                        #region Update Bedding Set
                        //try
                        //{
                        //    product.BodyHtml = beddingSet.Items.FirstOrDefault().BodyHtml;
                        //    //product.Title = FixTitle(product.Title);
                        //    product.Vendor = "Bedding Set";
                        //    List<ProductOption> productOptions = new List<ProductOption>();
                        //    //Values 
                        //    var valuesSizes = new List<string>();
                        //    valuesSizes.Add("US Twin");
                        //    valuesSizes.Add("US Full");
                        //    valuesSizes.Add("US Queen");
                        //    valuesSizes.Add("US King");
                        //    valuesSizes.Add("US Cal King");
                        //    var option = new ProductOption();
                        //    option.Name = "Size";
                        //    option.Values = valuesSizes;
                        //    productOptions.Add(option);
                        //    var allVariant = new List<ProductVariant>();
                        //    var variants = new List<ProductVariant>();
                        //    var firstVariant = product.Variants.FirstOrDefault();
                        //    foreach (var size in valuesSizes)
                        //    {
                        //        var newVariant = new ProductVariant();
                        //        switch (size)
                        //        {
                        //            case "US Twin":
                        //                newVariant.Price = 78.95m;
                        //                break;
                        //            case "US Full":
                        //                newVariant.Price = 89.99m;
                        //                break;
                        //            case "US Queen":
                        //                newVariant.Price = 99.99m;
                        //                break;
                        //            case "US King":
                        //                newVariant.Price = 109.99m;
                        //                break;
                        //            case "US Cal King":
                        //                newVariant.Price = 119.99m;
                        //                break;
                        //            default: break;
                        //        }

                        //        newVariant.Option1 = size;
                        //        newVariant.Option2 = null;
                        //        newVariant.Option3 = null;
                        //        newVariant.Position = firstVariant.Position;
                        //        newVariant.Barcode = firstVariant.Barcode;
                        //        newVariant.SKU = firstVariant.SKU + "-" + size;
                        //        newVariant.InventoryQuantity = 9999999;
                        //        newVariant.FulfillmentService = "Manual";
                        //        newVariant.Grams = firstVariant.Grams;
                        //        newVariant.ImageId = firstVariant.ImageId;
                        //        newVariant.InventoryPolicy = firstVariant.InventoryPolicy;
                        //        newVariant.CompareAtPrice = firstVariant.CompareAtPrice;
                        //        newVariant.Title = firstVariant.Title;
                        //        newVariant.Weight = firstVariant.Weight;
                        //        newVariant.WeightUnit = firstVariant.WeightUnit;
                        //        allVariant.Add(newVariant);
                        //    }
                        //    //foreach (var variant in product.Variants)
                        //    //    await _productVariantService.DeleteAsync(product.Id.Value, variant.Id.Value);

                        //    product.Options = productOptions;
                        //    product.Variants = allVariant;
                        //    var productUpdated = await productService.UpdateAsync(product.Id.Value, product);
                        //    totalSuccess++;
                        //    rtbLogTab3.Focus();
                        //    rtbLogTab3.AppendText(Environment.NewLine);
                        //    rtbLogTab3.AppendText($"- Updated: '{product.Title}' --- {totalSuccess}/{productCount} scanned product");
                        //}
                        //catch (Exception ex)
                        //{
                        //    rtbLogTab3.Focus();
                        //    rtbLogTab3.AppendText(Environment.NewLine);
                        //    rtbLogTab3.AppendText($"- Error: '{product.Title}' --- {totalSuccess}/{productCount} scanned product");
                        //    rtbLogTab3.AppendText(Environment.NewLine);
                        //    rtbLogTab3.AppendText($"-> Detail: '{ex.Message}'");
                        //}
                        #endregion
                        #region updateFaceProtection
                        //try
                        //{
                        //    if (product.Tags.ToLower().Contains("face-protection-updated-1"))
                        //        continue;
                        //    string des = string.Empty;
                        //    //check type
                        //    bool isFaceMask = !product.Title.ToLower().Contains("neck gaiter");
                        //    product.ProductType = isFaceMask ? "Face Mask" : "Neck Gaiter";
                        //    product.Vendor = isFaceMask ? "Face Mask TM" : "Neck Gaiter TM";
                        //    des = isFaceMask ? Common.LoadShopifyDescription("FaceMask") : Common.LoadShopifyDescription("NeckGaiter");
                        //    if (!string.IsNullOrEmpty(des))
                        //    {
                        //        string newTitle = Common.FixTitleBasic(product.Title);
                        //        product.Tags += ",face-protection-updated-1";
                        //        List<ProductOption> productOptions = new List<ProductOption>();
                        //        //Values 
                        //        var valuesPack = new List<string>();
                        //        valuesPack.Add("One Item");
                        //        if (!product.Title.ToUpper().Contains("PACK"))
                        //        {
                        //            valuesPack.Add("Pack 3");
                        //            valuesPack.Add("Pack 5");
                        //            valuesPack.Add("Pack 10");
                        //        }
                        //        var option = new ProductOption();
                        //        option.Name = "Pack";
                        //        option.Values = valuesPack;
                        //        productOptions.Add(option);
                        //        var allVariant = new List<ProductVariant>();
                        //        var variants = new List<ProductVariant>();
                        //        var firstVariant = product.Variants.FirstOrDefault();
                        //        foreach (var quantity in valuesPack)
                        //        {
                        //            var newVariant = new ProductVariant();
                        //            switch (quantity)
                        //            {
                        //                case "One Item":
                        //                    newVariant.Price = isFaceMask ? 15.99m : 18.99m;
                        //                    break;
                        //                case "Pack 3":
                        //                    newVariant.Price = isFaceMask ? 39.99m : 45.99m;
                        //                    break;
                        //                case "Pack 5":
                        //                    newVariant.Price = isFaceMask ? 59.99m : 59.99m;
                        //                    break;
                        //                case "Pack 10":
                        //                    newVariant.Price = isFaceMask ? 104.99m : 99.99m;
                        //                    break;
                        //                default: break;
                        //            }
                        //            if (product.Title.ToUpper().Contains("PACK"))
                        //            {
                        //                if (product.Title.Contains("2"))
                        //                    newVariant.Price = isFaceMask ? 30.99m : 30.99m;
                        //                if (product.Title.Contains("3"))
                        //                    newVariant.Price = isFaceMask ? 39.99m : 45.99m;
                        //                if (product.Title.Contains("5"))
                        //                    newVariant.Price = isFaceMask ? 59.99m : 59.99m;
                        //                if (product.Title.Contains("10"))
                        //                    newVariant.Price = isFaceMask ? 104.99m : 99.99m;
                        //            }
                        //            newVariant.Option1 = quantity;
                        //            newVariant.Option2 = null;
                        //            newVariant.Option3 = null;
                        //            newVariant.Position = firstVariant.Position;
                        //            newVariant.Barcode = firstVariant.Barcode;
                        //            newVariant.SKU = firstVariant.SKU + "-" + quantity;
                        //            newVariant.InventoryQuantity = 9999999;
                        //            newVariant.FulfillmentService = "Manual";
                        //            newVariant.Grams = firstVariant.Grams;
                        //            newVariant.ImageId = firstVariant.ImageId;
                        //            newVariant.InventoryPolicy = firstVariant.InventoryPolicy;
                        //            newVariant.CompareAtPrice = firstVariant.CompareAtPrice;
                        //            newVariant.Title = firstVariant.Title;
                        //            newVariant.Weight = firstVariant.Weight;
                        //            newVariant.WeightUnit = firstVariant.WeightUnit;
                        //            allVariant.Add(newVariant);
                        //        }
                        //        product.Options = productOptions;
                        //        product.Variants = allVariant;
                        //        await productService.UpdateAsync(product.Id.Value, new ShopifySharp.Product { BodyHtml = des, Title = newTitle, ProductType = product.ProductType, Tags = product.Tags, Options = productOptions, Variants = allVariant });
                        //        totalSuccess++;
                        //        if (product.Id.Value > maxProductIdUpdated)
                        //            Common.LogMaxProductIdDreamshipToShopify(product.Id.Value);

                        //        totalSuccess++;
                        //        rtbLogTab3.Focus();
                        //        rtbLogTab3.AppendText(Environment.NewLine);
                        //        rtbLogTab3.AppendText($"- Updated: '{product.Title}' --- {totalSuccess}/{productCount} scanned product");
                        //    }
                        //    else
                        //    {
                        //        // do nothing
                        //    }
                        //}
                        //catch
                        //{
                        //    // Do  nothing
                        //}
                        #endregion
                    }
                    catch (Exception ex)
                    {

                    }
                    #region demo
                    //    //For Tshirt
                    //    //if (product.Title.ToLower().Contains("long sleeve"))
                    //    //{
                    //    //    product.BodyHtml = longSleeve.Items.FirstOrDefault().BodyHtml;
                    //    //    await productService.UpdateAsync(product.Id.Value, product);
                    //    //    continue;
                    //    //}
                    //    //if (product.Title.ToLower().Contains("sweatshirt"))
                    //    //{
                    //    //    product.BodyHtml = sweatShirt.Items.FirstOrDefault().BodyHtml;
                    //    //    await productService.UpdateAsync(product.Id.Value, product);
                    //    //    continue;
                    //    //}
                    //    //if (product.Title.ToLower().Contains("guys tee"))
                    //    //{
                    ////    //    product.BodyHtml = guysTee.Items.FirstOrDefault().BodyHtml;
                    //    //    await productService.UpdateAsync(product.Id.Value, product);
                    //    //    continue;
                    //    //}
                    //    //if (product.Title.ToLower().Contains("guys v-neck"))
                    //    //{
                    //    //    product.BodyHtml = guysVNeck.Items.FirstOrDefault().BodyHtml;
                    //    //    await productService.UpdateAsync(product.Id.Value, product);
                    //    //    continue;
                    //    //}
                    //    //Update variant not track quantity
                    //    //foreach(var variant in product.Variants)
                    //    //{
                    //    //    variant.FulfillmentService = "Manual";
                    //    //    variant.CompareAtPrice = null;
                    //    //    variant.InventoryQuantity = null;
                    //    //}
                    //    if (!product.Images.Any())
                    //        await productService.DeleteAsync(product.Id.Value);
                    //}
                    //Clone
                    //var allProductItems = allProduct.Items;
                    //foreach (var product in allProductItems)
                    //{
                    //    productCount++;
                    //    if (product.Title.ToLower().Contains("high top") && !product.Title.ToLower().Contains("sneakers"))
                    //        product.BodyHtml = hightTop.Items.FirstOrDefault().BodyHtml;
                    //    if (product.Title.ToLower().Contains("low top") && !product.Title.ToLower().Contains("sneakers"))
                    //        product.BodyHtml = lowTop.Items.FirstOrDefault().BodyHtml;
                    //    if (product.Title.ToLower().Contains("sneakers") && !product.Title.ToLower().Contains("high top") && !product.Title.ToLower().Contains("low top"))
                    //        product.BodyHtml = sneaker.Items.FirstOrDefault().BodyHtml;
                    //    Thread.Sleep(1000);
                    //    var newProduct = await CloneTo(product, "https://ziposhoes.myshopify.com/admin/", "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");
                    //    if (newProduct != null)
                    //    {
                    //        totalSuccess++;
                    //        rtbLogTab3.Focus();
                    //        rtbLogTab3.AppendText(Environment.NewLine);
                    //        rtbLogTab3.AppendText($"- Imported: '{product.Title}' --- {totalSuccess}/{productCount} scanned product");
                    //    }
                    //    else
                    //    {
                    //        rtbLogTab3.Focus();
                    //        rtbLogTab3.AppendText(Environment.NewLine);
                    //        rtbLogTab3.AppendText($"- Already exist '{product.Title}' in the store --- {totalSuccess}/{productCount} scanned product");
                    //    }
                    #endregion
                }
                rtbLogTab3.Focus();
                rtbLogTab3.AppendText(Environment.NewLine);
                rtbLogTab3.AppendText($"- {totalSuccess} product updated!");
                if (!allProduct.HasNextPage)
                {
                    break;
                }
                allProduct = await productService.ListAsync(allProduct.GetNextPageFilter());
            }
            MessageBox.Show("Clone successfully", "Infomation!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public string ToTitleCase(string str)
        {
            string result = str;
            if (!string.IsNullOrEmpty(str))
            {
                var words = str.Split(' ');
                for (int index = 0; index < words.Length; index++)
                {
                    var s = words[index];
                    if (s.Length > 0)
                    {
                        words[index] = s[0].ToString().ToUpper() + s.Substring(1);
                    }
                }
                result = string.Join(" ", words);
            }
            return result;
        }
        public static async Task<ShopifySharp.Product> CloneTo(Product product, string adminSpfUrl, string adminSpfPassword)
        {
            var productService = new ProductService(adminSpfUrl, adminSpfPassword);
            productService.SetExecutionPolicy(new SmartRetryExecutionPolicy());
            var filter = new ProductListFilter()
            {
                Title = product.Title
            };
            var allProduct = await productService.ListAsync(filter);
            if (allProduct.Items.Any())
                return null;
            else
                Thread.Sleep(1000);
            //Create product with current information
            var _newProduct = new ShopifySharp.Product();
            _newProduct.Title = product.Title;
            _newProduct.BodyHtml = product.BodyHtml;
            _newProduct.ProductType = product.ProductType;
            _newProduct.Tags = product.Tags;
            _newProduct.Vendor = product.Vendor;
            _newProduct.Options = product.Options;
            _newProduct.Metafields = product.Metafields;
            var variants = new List<ProductVariant>();
            foreach (var variant in product.Variants)
            {
                var productVariant = new ProductVariant
                {
                    Price = variant.Price,
                    Title = variant.Title,
                    Option1 = variant.Option1,
                    Option2 = variant.Option2,
                    Option3 = variant.Option3,
                    Position = variant.Position,
                    SKU = variant.SKU,
                };
                variants.Add(productVariant);
            }
            _newProduct.Variants = variants;

            _newProduct = await productService.CreateAsync(_newProduct);

            var _imageService = new ProductImageService(adminSpfUrl, adminSpfPassword);
            //create product image
            foreach (var image in product.Images)
            {
                var productImage = new ProductImage
                {
                    Alt = image.Alt,
                    Attachment = image.Attachment,
                    Src = image.Src,
                    Metafields = image.Metafields
                };
                //select image variant matched
                if (image.VariantIds.Any())
                {
                    var oldVariants = product.Variants.Where(p => image.VariantIds.Contains(p.Id.Value)).ToList();
                    var imageVariantIds = new List<long>();
                    foreach (var oldVariant in oldVariants)
                    {
                        var newVariant = _newProduct.Variants.FirstOrDefault(p => p.Title == oldVariant.Title
                        && p.Option1 == oldVariant.Option1
                        && p.Option2 == oldVariant.Option2
                        && p.Option3 == oldVariant.Option3);
                        if (newVariant == null)
                            continue;
                        imageVariantIds.Add(newVariant.Id.Value);
                    }
                    productImage.VariantIds = imageVariantIds;
                }
                //creat image
                productImage = await _imageService.CreateAsync(_newProduct.Id.Value, productImage);
            }
            await productService.UnpublishAsync(_newProduct.Id.Value);
            return _newProduct;
        }

        private void btnPrepare_Click(object sender, EventArgs e)
        {
            btnBackupShopify.Hide();
            btnDeleteProductError.Hide();
            rtbLogTab4.Clear();
            rtbLogTab4.Text = $"Preparing.............";
            listStore = new List<StoreAuthencation>();
            #region prepare store 
            foreach (var store in storeTomeeySelected)
            {
                rtbListRepository.Focus();
                if (rtbListRepository.Lines.Count() > 0)
                    rtbListRepository.AppendText(Environment.NewLine);
                rtbListRepository.AppendText($"{store.Url}   {store.ApiKey}  {store.ApiPassword} {store.Name.Replace(" ", "-")}");
            }
            #endregion
            foreach (string line in rtbListRepository.Lines)
            {
                string[] args = line.Split('\t', ' ');
                args = args.Where(p => !string.IsNullOrEmpty(p)).ToArray();
                if (args.Count() == 4)
                {
                    rtbLogTab4.Focus();
                    rtbLogTab4.AppendText(Environment.NewLine);
                    rtbLogTab4.AppendText($"{args[0]}-*-{args[1]}-*-{args[2]}-*-{args[3]}");
                    listStore.Add(new StoreAuthencation
                    {
                        Url = args[0],
                        Api = args[1],
                        Pass = args[2],
                        StoreName = args[3]
                    });
                }
                else
                {
                    rtbLogTab4.Focus();
                    rtbLogTab4.AppendText(Environment.NewLine);
                    rtbLogTab4.AppendText($"Error read line: {line}");
                }
            }
            rtbLogTab4.Focus();
            rtbLogTab4.AppendText(Environment.NewLine);
            rtbLogTab4.AppendText($"Prepared {listStore.Count} store!");
            if (listStore.Any())
            {
                btnBackupShopify.Show();
                btnDeleteProductError.Show();
                btnProductCount.Show();
            }
        }
        public class StoreAuthencation
        {
            public string Url { get; set; }
            public string Api { get; set; }
            public string Pass { get; set; }
            public string StoreName { get; set; }
        }

        [Obsolete]
        private async void btnBackupShopify_Click(object sender, EventArgs e)
        {
            TurnOffButtons();
            btnExportShopbaseCSV.Hide();
            listProductPrepare = new List<ShopifyCustomService.Product>();
            string folderBackup = "shopify-backup/";
            folderBackup += string.Format("{0}-{1}-{2}-{3}-{4}-{5}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            if (!string.IsNullOrEmpty(txtTitle.Text))
            {
                folderBackup += "-" + txtTitle.Text;
            }
            if (!string.IsNullOrEmpty(txtProductType.Text))
            {
                folderBackup += "-" + txtProductType.Text;
            }
            if (Directory.Exists(folderBackup))
            {
                new System.IO.DirectoryInfo(folderBackup).Delete(true);
            }
            Directory.CreateDirectory(folderBackup);
            int totalProduct = 0;
            int totalProductUpdated = 0;
            int indexFile = 1;
            int packageSize = 1000;
            var updatedAtMin = DateTime.Now.AddYears(-1);
            var packageProducts = new List<ShopifyCustomService.Product>();
            string keySearch = txtTitle.Text;
            bool isFilterTitle = string.IsNullOrEmpty(keySearch) ? false : true;
            #region preapare products template
            string myShopbaseAdminUrlTemplate = "https://zolo-gifts.onshopbase.com/admin/";
            string shopApiKeyTemplate = "337ce770cb1000bb0ad98c0756c8ea32";
            string shopbaseAccessTokenTemplate = "11986e2cf2be19da3290e3e01001c6bfa37ca9dccb7123231cc9fcf7c518fb86";
            var serviceTemplate = new ShopbaseService.ProductService(myShopbaseAdminUrlTemplate, shopbaseAccessTokenTemplate, shopApiKeyTemplate);
            #region default products
            //var defaultHoodie = await serviceTemplate.GetAsync(1000000130553820);
            //var defaultSweatshirt = await serviceTemplate.GetAsync(1000000130554446);
            //var defaultLongSleeve = await serviceTemplate.GetAsync(1000000130554441);
            //var defaultTankTop = await serviceTemplate.GetAsync(1000000130554737);
            //var defaultGuysTee = await serviceTemplate.GetAsync(1000000130554444);
            //var defaultLadieTee = await serviceTemplate.GetAsync(1000000130554443);
            //var defaultGuysVneck = await serviceTemplate.GetAsync(1000000130554445);
            //var defaultLadiesVneck = await serviceTemplate.GetAsync(1000000130554442);
            //var defaultPhoneCase = await serviceTemplate.GetAsync(1000000130554440);
            //var defaultMug = await serviceTemplate.GetAsync(1000000076185729);
            //var defaultMatteCanvas = await serviceTemplate.GetAsync(1000000130555567);
            //var defaultPoster = await serviceTemplate.GetAsync(1000000130555662);
            //var defaultBlanket = await serviceTemplate.GetAsync(1000000120813764);
            //var defaultLadiesFlowyTank = await serviceTemplate.GetAsync(1000000130555406);
            //var defaultProduct = await serviceTemplate.GetAsync(1000000115878085);
            //var defaultVariants = new List<ShopifyCustomService.ProductVariant>();
            //var defaultOptions = new List<ShopifyCustomService.ProductOption>();
            //foreach (var varinant in defaultProduct.Variants)
            //{
            //    defaultVariants.Add(new ShopifyCustomService.ProductVariant
            //    {
            //        AdminGraphQLAPIId = varinant.AdminGraphQLAPIId,
            //        Barcode = varinant.Barcode,
            //        CompareAtPrice = varinant.CompareAtPrice,
            //        Grams = varinant.Grams,
            //        Title = varinant.Title,
            //        Option1 = varinant.Option1,
            //        Option2 = varinant.Option2,
            //        Option3 = varinant.Option3,
            //        Price = varinant.Price,
            //        SKU = varinant.SKU,
            //        TaxCode = varinant.TaxCode,
            //        Position = varinant.Position
            //    });
            //}
            //foreach (var option in defaultProduct.Options)
            //{
            //    defaultOptions.Add(new ShopifyCustomService.ProductOption
            //    {
            //        Values = option.Values,
            //        Position = option.Position,
            //        Name = option.Name,
            //        AdminGraphQLAPIId = option.AdminGraphQLAPIId
            //    });
            //}
            #endregion
            #endregion
            foreach (var store in listStore)
            {
                try
                {
                    string myShopifyUrl = store.Url;
                    string shopAccessToken = store.Pass;
                    var service = new ShopifyCustomService.ProductService(myShopifyUrl, shopAccessToken);
                    service.SetExecutionPolicy(new ShopifyCustomService.SmartRetryExecutionPolicy());
                    var serviceVariant = new ShopifyCustomService.ProductVariantService(myShopifyUrl, shopAccessToken);
                    serviceVariant.SetExecutionPolicy(new ShopifyCustomService.SmartRetryExecutionPolicy());
                    if (!string.IsNullOrEmpty(txtProductType.Text))
                    {
                        var listProductType = txtProductType.Text.Split(',');
                        foreach (string productType in listProductType)
                        {
                            rtbLogTab4.Focus();
                            rtbLogTab4.AppendText(Environment.NewLine);
                            rtbLogTab4.AppendText($"Scaning products buy '{productType}' in store '{store.Url}'.....");
                            lblLogRestoreShopify.Text = string.Format("Total product: {0} - Scanning '{1}' : '{2}'", totalProduct, productType, store.Url);
                            lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                            int productCount = 0;
                            await Task.Run(async () =>
                            {
                                int pageLimit = 50;
                                int pageIndex = 1;
                                var filter = new ShopifyCustomService.Filters.ProductFilter
                                {
                                    PublishedStatus = "published",
                                    Limit = pageLimit,
                                    ProductType = productType,
                                    Page = pageIndex
                                };
                                if (isFilterTitle)
                                    filter.Title = keySearch;
                                var products = await service.ListAsync(filter);
                                while (true)
                                {
                                    if (products.Count() == 0)
                                    {
                                        Invoke(new Action(() =>
                                        {
                                            lblLogRestoreShopify.Text = string.Format("Total product: {0} - Scanning '{1}' : '{2}' - Limit Page: {3} - Page Index: {4}", totalProduct, productType, store.Url, pageLimit, pageIndex);
                                            lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                                        }));
                                        break;
                                    }
                                    else
                                    {
                                        totalProduct += products.Count();
                                        productCount += products.Count();
                                        packageProducts.AddRange(products);
                                        listProductPrepare.AddRange(products);
                                        if (packageProducts.Count >= packageSize)
                                        {
                                            var takeProduct = packageProducts.Take(packageSize);
                                            packageProducts = packageProducts.Skip(packageSize).ToList();
                                            string jsonResult = JsonConvert.SerializeObject(takeProduct);
                                            using (System.IO.StreamWriter file =
                                            new System.IO.StreamWriter(string.Format("{0}/product-{1}-{2}-{3}.txt", folderBackup, pageIndex, indexFile, takeProduct.Count()), false))
                                            {
                                                file.WriteLine(jsonResult);
                                            }
                                            //ExportProductToShopbase();
                                        }
                                        pageIndex++;
                                        indexFile++;
                                    }
                                    if (products.Count() < pageLimit)
                                    {
                                        pageIndex = 1;
                                        Invoke(new Action(() =>
                                        {
                                            lblLogRestoreShopify.Text = string.Format("Total product: {0} - Scanned: '{1}' - Limit Page: {2} - Page Index: {3}", totalProduct, store.Url, pageLimit, pageIndex);
                                            lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                                        }));
                                        break;
                                    }
                                    pageIndex++;
                                    filter.Page = pageIndex;
                                    products = await service.ListAsync(filter);
                                    Invoke(new Action(() =>
                                    {
                                        lblLogRestoreShopify.Text = string.Format("Total product: {0} - Scanning '{1}' : '{2}' - Limit Page: {3} - Page Index: {4}", totalProduct, productType, store.Url, pageLimit, pageIndex);
                                        lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                                    }));
                                }
                            });
                            rtbLogTab4.Focus();
                            rtbLogTab4.AppendText(Environment.NewLine);
                            rtbLogTab4.AppendText($"Has backed up product type '{productType}' in store '{store.Url}' with {productCount} product.");
                            lblLogRestoreShopify.Text = string.Format("Total product: {0} - Scanned '{1}' : '{2}'", totalProduct, productType, store.Url);
                            lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                        }
                    }
                    else
                    {
                        int productCount = 0;
                        await Task.Run(async () =>
                        {
                            int pageLimit = 100;
                            int pageIndex = 1;
                            long sinceId = 0;
                            var filter = new ShopifyCustomService.Filters.ProductFilter
                            {
                                PublishedStatus = "unpublished",
                                Limit = pageLimit,
                                //CreatedAtMax = updatedAtMin,
                                //Tags = "Tracksuit-Ray",
                                //UpdatedAtMin = updatedAtMin,
                                SinceId = sinceId,
                                //Vendor = "Dreamship",
                                //Title = "curtain",
                                //ProductType = "Football Training Tracksuit"
                            };
                            if (isFilterTitle)
                                filter.Title = keySearch;
                            var products = await service.ListAsync(filter);
                            int totalProducScaned = 0;
                            while (true)
                            {
                                if (products.Count() == 0)
                                {
                                    Invoke(new Action(() =>
                                    {
                                        sinceId = 0;
                                        lblLogRestoreShopify.Text = string.Format("Total product: {0} - Scanning '{1}' - Limit Page: {2} - Page Index: {3}", totalProduct, store.Url, pageLimit, pageIndex);
                                        lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                                    }));
                                    break;
                                }
                                else
                                {
                                    var prepareListProducts = new List<ShopifyCustomService.Product>();
                                    string tagUpdate = "imported-21-5";
                                    //var productsToUpdate = products;
                                    var productsToUpdate = products.Where(p => p.Title.ToLower().Contains(" mat ") || p.Title.ToLower().Contains(" curtain") || p.Title.ToLower().Contains(" car seat"));
                                    //|| p.Tags.ToLower().Contains("trucker hat"));
                                    //var productsToUpdate = products.Where(p=>p.Title.ToLower().Contains("women tank top"));
                                    //var productsToUpdate = products.Where(p => p.UpdatedAt > updatedAtMin);
                                    //var productsToUpdate = products.Where(p => p.PublishedScope != "web");
                                    sinceId = products.Max(p => p.Id.Value);
                                    #region update products
                                    var testBool = true;
                                    foreach (var product in productsToUpdate)
                                    {
                                        testBool = !testBool;
                                        totalProducScaned++;
                                        if (!product.Images.Any())
                                        {
                                            await service.DeleteAsync(product.Id.Value);
                                            productsToUpdate = productsToUpdate.Where(p => !p.Id.Equals(product.Id));
                                            continue;
                                        }
                                        //if (product.Tags.ToLower().Contains("Storage Bin_Art thuong"))
                                        //    product.Handle += "-storege-bin";
                                        //if (product.Tags.Contains("Christmas Stocking"))
                                        //    product.Handle += "-christmas-stocking";
                                        //if (product.Images.Count() < 2 && product.Options.Any(p => p.Name.ToLower().Contains("color") && p.Values.Count() > 1))
                                        //{
                                        //    await service.DeleteAsync(product.Id.Value);
                                        //    continue;
                                        //}
                                        ///product.Vendor = store.StoreName;
                                        //if (store.StoreName.ToLower().Contains("runner"))
                                        //    product.Tags += ",tablerunner99";
                                        //if (store.StoreName.ToLower().Contains("tablecloth"))
                                        //    product.Tags += ",tablecloth92";
                                        //if (store.StoreName.ToLower().Contains("belt"))
                                        //    product.Tags += ",sbelt1207";
                                        //if (store.StoreName.ToLower().Contains("sticker"))
                                        //    product.Tags += ",stk8";
                                        //if (store.StoreName.ToLower().Contains("truck"))
                                        //    product.Tags += ",truck1065";
                                        //product.Tags += ",imported-";
                                        //product.Tags += ",tommey-01-06-2022";
                                        //product.Tags += ",sunshade-030121";
                                        //if (store.StoreName.Contains("02"))
             
                                        //    product.Tags += ",stocking_0910";
                                        //else
                                        //    product.Tags += ",tablecloth_0910";
                                        //product.ProductType += "NHL Jersey";
                                        //if (product.ProductType.ToLower().Contains("blanket"))
                                        //    product.Tags += ",blanket2021-gethinh";
                                        //else
                                        //    product.Tags += ",arearug2021-gethinh";
                                        //if (product.Title.ToLower().Contains("women"))
                                        //    product.ProductType += "NHL Women Jersey";
                                        //if (product.Title.ToLower().Contains("earrings"))
                                        //    foreach (var variant in product.Variants)
                                        //        variant.Price = 65.99m;
                                        //if (product.Title.ToLower().Contains("necklace") && product.Title.ToLower().Contains("bracelet"))
                                        //    foreach (var variant in product.Variants)
                                        //        variant.Price = 160.99m;

                                        //if (product.Tags.Contains("imported"))
                                        //    continue;
                                        //if (testBool)
                                        //    tagUpdate = "imported-baten";
                                        //else
                                        //    tagUpdate = "imported-zolo";
                                        //product.Title = FixTitle(product.Title);
                                        //if (!product.BodyHtml.Contains("All in stock items will ship within 10 days."))
                                        //    continue;
                                        //product.BodyHtml = product.BodyHtml.Replace("All in stock items will ship within 10 days. ", "");
                                        //product.Title = product.Title.Replace("Kids", "");
                                        product.Tags += "," + store.StoreName;
                                        product.Tags += "," + tagUpdate;
                                        product.Tags += ",imported-" + DateTime.UtcNow.ToString("MM-dd-yyyy");
                                        //product.ProductType = "3D Tablecloth";
                                        //product.Vendor = "Tablecloth P6";
                                        //if (!product.Tags.Contains(product.Id.Value.ToString()))
                                        //    product.Tags += ",id-" + product.Id.Value;
                                        //if (!product.Tags.Contains(product.Vendor))
                                        //    product.Tags += "," + product.Vendor;
                                        //var productUpdated = await service.UpdateAsync(product.Id.Value, product);
                                        //MarkAsCompletedProduct(product.Id.Value);
                                        //await service.PublishAsync(product.Id.Value);
                                        //prepareListProducts.Add(productUpdated);
                                        totalProductUpdated++;
                                        Invoke(new Action(() =>
                                        {
                                            rtbLogTab4.Focus();
                                            rtbLogTab4.AppendText(Environment.NewLine);
                                            rtbLogTab4.AppendText($"- Updated: '{product.Title}' --- {totalProductUpdated}/{totalProducScaned} scanned product");
                                        }));
                                    }
                                    #endregion
                                    var productToTake = productsToUpdate;

                                    totalProduct += productToTake.Count();
                                    productCount += productToTake.Count();
                                    packageProducts.AddRange(productToTake);
                                    listProductPrepare.AddRange(productToTake);
                                    if (packageProducts.Count >= packageSize)
                                    {
                                        var takeProduct = packageProducts.Take(packageSize);
                                        packageProducts = packageProducts.Skip(packageSize).ToList();
                                        string jsonResult = JsonConvert.SerializeObject(takeProduct);
                                        using (System.IO.StreamWriter file =
                                        new System.IO.StreamWriter(string.Format("{0}/product-{1}-{2}-{3}.txt", folderBackup, pageIndex, indexFile, takeProduct.Count()), false))
                                        {
                                            file.WriteLine(jsonResult);
                                        }
                                        //ExportProductToShopbase();
                                    }
                                    pageIndex++;
                                    indexFile++;
                                }
                                if (products.Count() < pageLimit)
                                {
                                    sinceId = 0;
                                    pageIndex = 1;
                                    Invoke(new Action(() =>
                                    {
                                        lblLogRestoreShopify.Text = string.Format("Total product: {0} - Scanned: '{1}' - Limit Page: {2} - Page Index: {3}", totalProduct, store.Url, pageLimit, pageIndex);
                                        lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                                    }));
                                    break;
                                }
                                //filter.Page = pageIndex;
                                filter.SinceId = sinceId;
                                products = await service.ListAsync(filter);
                                Invoke(new Action(() =>
                                {
                                    lblLogRestoreShopify.Text = string.Format("Total product: {0} - Scanning '{1}' - Limit Page: {2} - Page Index: {3}", totalProduct, store.Url, pageLimit, pageIndex);
                                    lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                                }));
                            }
                        });
                        rtbLogTab4.Focus();
                        rtbLogTab4.AppendText(Environment.NewLine);
                        rtbLogTab4.AppendText($"Has backed up products in store '{store.Url}' with {productCount} product.");
                    }
                }
                catch (Exception ex)
                {
                    rtbLogTab4.Focus();
                    rtbLogTab4.AppendText(Environment.NewLine);
                    rtbLogTab4.AppendText($"Error: {ex.Message} - {store.Url}");
                }
                if (listStore.IndexOf(store) == listStore.Count - 1 && packageProducts.Any())
                {
                    string jsonResult = JsonConvert.SerializeObject(packageProducts);
                    using (System.IO.StreamWriter file =
                    new System.IO.StreamWriter(string.Format("{0}/product-{1}-{2}-end-total-{3}.txt", folderBackup, indexFile, packageProducts.Count(), totalProduct), false))
                    {
                        file.WriteLine(jsonResult);
                    }
                    packageProducts = new List<ShopifyCustomService.Product>();
                    //ExportProductToShopbase();
                }
            }

            rtbLogTab4.Focus();
            rtbLogTab4.AppendText(Environment.NewLine);
            rtbLogTab4.AppendText($"Completed! {totalProduct} has been backed up!");
            if (listProductPrepare.Any())
            {
                btnExportShopbaseCSV.Show();
            }
            TurnOnButtons();
            MessageBox.Show("Backup successfully!", "Infomation!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        public static void MarkAsCompletedProduct(long id)
        {
            using (bonzaroEntities db = new bonzaroEntities())
            {
                var current = db.ShopifyImportProducts.FirstOrDefault(p => p.ProductId == id);
                if (current == null)
                    return;
                if (current.Status == (int)Common.ShopifyImportStatus.Completed)
                    return;
                current.Status = (int)Common.ShopifyImportStatus.Completed;
                bool saved = false;
                int retry = 0;
                while (!saved)
                {
                    try
                    {
                        db.SaveChanges();
                        saved = true;
                    }
                    catch (Exception ex)
                    {
                        if (retry == 3)
                            throw ex;
                        retry++;
                        //sleep 5s and continue
                        Thread.Sleep(5000);
                    }
                }
            }
        }
        private List<ShopifyCustomService.Product> GetProductsToTake(List<ShopifyCustomService.Product> products)
        {
            try
            {
                var results = new List<ShopifyCustomService.Product>();
                string title = "Blanket,Flag,Neck Gaiter,Face Cover,Face Mask,Poster,Canvas";
                var listArrTitle = title.Split(',');
                foreach (var condition in listArrTitle)
                {
                    var productToTake = products.Where(p => p.Title.ToLower().Contains(condition.ToLower())).ToList();
                    results.AddRange(productToTake);
                }
                string type = "Matte Canvas,Framed Matte Canvas,Peel & Stick Poster,Vertical Poster,Horizontal Poster,Sherpa Blanket,Fleece Blanket";
                var listArrType = type.Split(',');
                foreach (var condition in listArrType)
                {
                    var productToTake = products.Where(p => p.ProductType.ToLower().Contains(condition.ToLower())).ToList();
                    results.AddRange(productToTake);
                }
                results = results.Distinct().ToList();
                return results;
            }
            catch { }
            return null;
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var itemSelected = comboBox1.SelectedItem as Shopbase_Store;
            if (itemSelected != null)
                storeSelected = itemSelected.Id;
        }
        private void checkedListStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            storeTomeeySelected = new List<ShopifyCenterStore>();
            for (int i = 0; i < checkedListStore.CheckedItems.Count; i++)
            {
                storeTomeeySelected.Add(checkedListStore.CheckedItems[i] as ShopifyCenterStore);
            }
        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var itemSelected = comboBox2.SelectedItem as Shopbase_Store;
            if (itemSelected != null)
                storeSelectedTab5 = itemSelected.Id;
        }
        private async void btnRestoreShopify_Click(object sender, EventArgs e)
        {
            TurnOffButtons();
            if (storeSelected > 0)
            {
                var store = db.Shopbase_Stores.FirstOrDefault(p => p.Id == storeSelected);
                if (store == null)
                {
                    MessageBox.Show("Can not get store!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string myShopifyUrl = store.AdminUrl;
                string shopAccessToken = store.Password;
                string folderBackupShopify = string.Empty;
                var service = new ProductService(myShopifyUrl, shopAccessToken);
                service.SetExecutionPolicy(new SmartRetryExecutionPolicy());

                var serviceProductImage = new ProductImageService(myShopifyUrl, shopAccessToken);
                serviceProductImage.SetExecutionPolicy(new SmartRetryExecutionPolicy());

                var serviceProductVariant = new ProductVariantService(myShopifyUrl, shopAccessToken);
                serviceProductVariant.SetExecutionPolicy(new SmartRetryExecutionPolicy());

                var collectionService = new CustomCollectionService(myShopifyUrl, shopAccessToken);


                //var resultService = await service.GetAsync(2017446723647);

                var collects = await collectionService.ListAsync();
                long collectDefaultId = 0;
                if (collects.Items.Count() > 0)
                    collectDefaultId = collects.Items.First().Id.Value;
                else
                {
                    var customCollect = await collectionService.CreateAsync(new CustomCollection
                    {
                        Title = "Home",
                    });
                    collectDefaultId = customCollect.Id.Value;
                }


                //folderBackupShopify = @"C:\Workspace\tool-import-product-nop\EggStoreImportProduct\bin\Debug\shopify-backup\2019-9-27";

                using (var fldrDlg = new FolderBrowserDialog())
                {

                    if (fldrDlg.ShowDialog() == DialogResult.OK)
                    {
                        folderBackupShopify = fldrDlg.SelectedPath;
                    }
                }
                string folderBackupError = string.Format("{0}/error-{1}-{2}-{3}", folderBackupShopify, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                if (Directory.Exists(folderBackupError))
                {
                    new System.IO.DirectoryInfo(folderBackupError).Delete(true);
                }
                Directory.CreateDirectory(folderBackupError);

                string folderBackupDone = string.Format("{0}/Done", folderBackupShopify);
                string folderLogRestore = string.Format("{0}/Log", folderBackupShopify);

                if (!Directory.Exists(folderBackupDone))
                {
                    Directory.CreateDirectory(folderBackupDone);
                }
                if (!Directory.Exists(folderLogRestore))
                {
                    Directory.CreateDirectory(folderLogRestore);
                }

                lblFolderRestore.Text = folderBackupShopify;

                await Task.Run(async () =>
                {
                    if (!string.IsNullOrEmpty(folderBackupShopify))
                    {
                        int totalSuccess = 0;
                        int totalError = 0;

                        DirectoryInfo di = new DirectoryInfo(folderBackupShopify);
                        var allFilesBackup = di.GetFiles();
                        foreach (var fileBackup in allFilesBackup)
                        {
                            string contentJson = string.Empty;
                            var fileStream = new FileStream(fileBackup.FullName, FileMode.Open, FileAccess.Read);
                            using (var streamReader = new StreamReader(fileStream))
                            {
                                contentJson = streamReader.ReadToEnd();
                            }
                            Invoke(new Action(() =>
                            {
                                rtbLogTab4.Focus();
                                rtbLogTab4.AppendText(Environment.NewLine);
                                rtbLogTab4.AppendText($"-Preparing file: {fileBackup.FullName}");
                            }));
                            var productsInFiles = JsonConvert.DeserializeObject<List<ShopifySharp.Product>>(contentJson);
                            var lstProductRestoreError = new List<ShopifySharp.Product>();
                            string fileLogIndexRestore = string.Format("{0}/{1}", folderLogRestore, fileBackup.Name);

                            int indexProductRestore = LoadIndexRestoreShopify(fileLogIndexRestore) + 1;

                            for (int i = indexProductRestore; i < productsInFiles.Count(); i++)
                            {
                                var product = productsInFiles[i];

                                long shopifyProductId = 0;

                                try
                                {
                                    var filter = new ProductListFilter()
                                    {
                                        Title = product.Title
                                    };
                                    var allProduct = await service.ListAsync(filter);
                                    if (allProduct.Items.Any())
                                    {
                                        try
                                        {
                                            //var productToUpdate = allProduct.Items.FirstOrDefault(p => p.Title == product.Title);
                                            //if (productToUpdate == null)
                                            //    continue;
                                            //productToUpdate.Title = productToUpdate.Title.Replace("100% ", "");
                                            //productToUpdate.BodyHtml = product.BodyHtml;
                                            //var productUpdated = await service.UpdateAsync(allProduct.Items.FirstOrDefault().Id.Value, productToUpdate);
                                            totalError++;
                                            Invoke(new Action(() =>
                                            {
                                                rtbLogTab4.Focus();
                                                rtbLogTab4.AppendText(Environment.NewLine);
                                                rtbLogTab4.AppendText($"-{totalError} error, already exist '{product.Title}' in the store.");
                                            }));
                                        }
                                        catch (Exception ex) { }
                                        continue;
                                    }
                                    ShopifySharp.Product productRestore = new ShopifySharp.Product();
                                    productRestore.Title = product.Title;
                                    productRestore.BodyHtml = product.BodyHtml;
                                    productRestore.ProductType = product.ProductType;
                                    productRestore.Tags = product.Tags;
                                    productRestore.PublishedScope = "web";
                                    productRestore.PublishedAt = DateTime.UtcNow;
                                    List<ProductOption> productOptionRestore = new List<ProductOption>();
                                    foreach (ProductOption opt in product.Options)
                                    {
                                        ProductOption tmpOption = new ProductOption();
                                        tmpOption.Name = opt.Name;
                                        tmpOption.Position = opt.Position;
                                        List<string> tmpValue = new List<string>();
                                        foreach (string valueOpt in opt.Values)
                                        {
                                            tmpValue.Add(valueOpt);
                                        }
                                        tmpOption.Values = tmpValue;
                                        productOptionRestore.Add(tmpOption);
                                    }
                                    //productRestore.Options = productOptionRestore;
                                    productRestore = await service.CreateAsync(productRestore, new ProductCreateOptions() { Published = true });

                                    shopifyProductId = productRestore.Id.Value;

                                    // Add Product Image
                                    List<RestoreShopifyProductImage> lstRestoreProductImages = new List<RestoreShopifyProductImage>();
                                    foreach (var prImage in product.Images)
                                    {
                                        var tmpImage = await serviceProductImage.CreateAsync(productRestore.Id.Value, new ProductImage { Src = prImage.Src });
                                        lstRestoreProductImages.Add(new RestoreShopifyProductImage { OldImageId = prImage.Id.Value, NewImageId = tmpImage.Id.Value });
                                        Thread.Sleep(500);
                                    }

                                    //await service.UpdateAsync(productRestore.Id.Value, new ShopifySharp.Product { Options = productOptionRestore });
                                    List<ProductVariant> productVariants = new List<ProductVariant>();
                                    // Add Product variant
                                    foreach (var prVariant in product.Variants)
                                    {
                                        ProductVariant tmpVariant = new ProductVariant();
                                        tmpVariant.Option1 = prVariant.Option1;
                                        tmpVariant.Option2 = prVariant.Option2;
                                        tmpVariant.Option3 = prVariant.Option3;
                                        tmpVariant.Price = prVariant.Price;
                                        tmpVariant.InventoryManagement = prVariant.InventoryManagement;
                                        tmpVariant.InventoryPolicy = prVariant.InventoryPolicy;
                                        tmpVariant.SKU = prVariant.SKU;
                                        tmpVariant.FulfillmentService = "Manual";

                                        //tmpVariant = await serviceProductVariant.CreateAsync(productRestore.Id.Value, tmpVariant);

                                        // Add image to variant
                                        if (prVariant.ImageId != null)
                                        {
                                            var imageBackup = lstRestoreProductImages.Where(im => im.OldImageId == prVariant.ImageId.Value).FirstOrDefault();
                                            //await serviceProductVariant.UpdateAsync(prVariant.Id.Value, new ProductVariant { ImageId = imageBackup.NewImageId });
                                            if (imageBackup != null)
                                                tmpVariant.ImageId = imageBackup.NewImageId;
                                        }
                                        else
                                        {
                                            tmpVariant.ImageId = lstRestoreProductImages.FirstOrDefault().NewImageId;
                                        }
                                        productVariants.Add(tmpVariant);
                                    }


                                    //await service.UpdateAsync(productRestore.Id.Value, new ShopifySharp.Product { Options = productOptionRestore.ToArray(), Variants = productVariants.ToArray() });
                                    // update first variants
                                    await service.UpdateAsync(productRestore.Id.Value, new ShopifySharp.Product { Options = productOptionRestore.ToArray(), Variants = productVariants.Take(1).ToArray() });

                                    for (int indexVariant = 1; indexVariant < productVariants.Count(); indexVariant++)
                                    {
                                        await serviceProductVariant.CreateAsync(shopifyProductId, productVariants[indexVariant]);
                                        Thread.Sleep(200);
                                    }

                                    //Thread.Sleep(5000);
                                    // add product to default collection
                                    try
                                    {
                                        var collectService = new CollectService(myShopifyUrl, shopAccessToken);
                                        collectionService.SetExecutionPolicy(new SmartRetryExecutionPolicy());

                                        await collectService.CreateAsync(new ShopifySharp.Collect
                                        {
                                            ProductId = productRestore.Id.Value,
                                            CollectionId = collectDefaultId
                                        });
                                    }
                                    catch
                                    {
                                        // do no thing
                                    }

                                    // publish product
                                    await service.UnpublishAsync(shopifyProductId);

                                    totalSuccess++;

                                    Invoke(new Action(() =>
                                    {
                                        lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                                        rtbLogTab4.Focus();
                                        rtbLogTab4.AppendText(Environment.NewLine);
                                        rtbLogTab4.AppendText($"-{totalSuccess} success, imported '{product.Title}'.");
                                    }));

                                    LogIndexRestoreShopify(fileLogIndexRestore, i);

                                }
                                catch (Exception ex)
                                {
                                    lstProductRestoreError.Add(product);
                                    totalError++;
                                    if (shopifyProductId != 0)
                                    {
                                        try
                                        {
                                            await service.DeleteAsync(shopifyProductId);
                                        }
                                        catch
                                        {
                                            // do nothing
                                        }

                                    }
                                    Invoke(new Action(() =>
                                    {
                                        rtbLogTab4.Focus();
                                        rtbLogTab4.AppendText(Environment.NewLine);
                                        rtbLogTab4.AppendText($"-{totalError} error, there was an error importing product {product.Title} into store:");
                                        rtbLogTab4.AppendText(Environment.NewLine);
                                        rtbLogTab4.AppendText(ex.Message);
                                    }));
                                }
                                finally
                                {
                                    Invoke(new Action(() =>
                                    {
                                        lblLogRestoreShopify.Text = string.Format("Success: {0} - Error: {1} - Index: {2} - Page: {3}", totalSuccess, totalError, i, fileBackup.Name);
                                    }));
                                }
                            }

                            if (lstProductRestoreError.Count() > 0)
                            {
                                string jsonResult = JsonConvert.SerializeObject(lstProductRestoreError);
                                using (System.IO.StreamWriter file =
                                new System.IO.StreamWriter(string.Format("{0}/{1}", folderBackupError, fileBackup.Name), false))
                                {
                                    file.WriteLine(jsonResult);
                                }
                            }

                            // move file backup to done
                            File.Move(fileBackup.FullName, string.Format("{0}/{1}", folderBackupDone, fileBackup.Name));
                        }
                    }
                });
                var totalProduct = await service.CountAsync();
                store.PhoneNumber = totalProduct.ToString();
                db.SaveChanges();
            }
            else
            {
                MessageBox.Show("You need select a store!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            TurnOnButtons();
        }
        #region Helper
        protected static int LoadIndexRestoreShopify(string fileName)
        {
            int result = 0;
            try
            {
                System.IO.StreamReader file =
                new System.IO.StreamReader(fileName);
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    result = Int32.Parse(line);
                }

                file.Close();
            }
            catch
            {
                result = 0;
            }
            return result;
        }
        protected static void LogIndexRestoreShopify(string fileName, int index)
        {
            using (StreamWriter newTask = new StreamWriter(fileName, false))
            {
                newTask.WriteLine(index);
            }
        }
        class RestoreShopifyProductImage
        {
            public long OldImageId { get; set; }
            public long NewImageId { get; set; }
        }
        #endregion

        private async void btnDeleteProductError_Click(object sender, EventArgs e)
        {
            TurnOffButtons();
            int totalProductDeleted = 0;
            foreach (var store in listStore)
            {
                try
                {
                    rtbLogTab4.Focus();
                    rtbLogTab4.AppendText(Environment.NewLine);
                    rtbLogTab4.AppendText($"******Scanning store: '{store.Url}'!******");
                    string myShopifyUrl = store.Url;
                    string shopAccessToken = store.Pass;
                    var service = new ProductService(myShopifyUrl, shopAccessToken);
                    service.SetExecutionPolicy(new SmartRetryExecutionPolicy());
                    int productCount = 0;
                    await Task.Run(async () =>
                    {
                        int pageLimit = 50;
                        int pageIndex = 1;
                        var filter = new ProductListFilter
                        {
                            Limit = pageLimit
                        };
                        var products = await service.ListAsync(filter);
                        while (true)
                        {
                            if (products.Items.Count() == 0)
                                break;
                            else
                            {
                                var productNoneImages = products.Items.Where(p => !p.Images.Any());
                                foreach (var product in productNoneImages)
                                {
                                    await service.DeleteAsync(product.Id.Value);
                                    productCount++;
                                    totalProductDeleted++;
                                    Invoke(new Action(() =>
                                    {
                                        rtbLogTab4.Focus();
                                        rtbLogTab4.AppendText(Environment.NewLine);
                                        rtbLogTab4.AppendText($"Deleted productId: {product.Id.Value}: {productCount} / {totalProductDeleted} - {store.Url}");
                                        lblLogRestoreShopify.Text = string.Format("Total Removed: {0} - Scanning: '{1}' - Limit Page: {2} - Page Index: {3}", totalProductDeleted, store.Url, pageLimit, pageIndex);
                                        lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                                    }));
                                }
                            }
                            if (!products.HasNextPage)
                            {
                                Invoke(new Action(() =>
                                {
                                    rtbLogTab4.Focus();
                                    rtbLogTab4.AppendText(Environment.NewLine);
                                    rtbLogTab4.AppendText($"******Scanned store: '{store.Url}' and {productCount} product in store removed!******");
                                }));
                                break;
                            }
                            products = await service.ListAsync(products.GetNextPageFilter());
                            pageIndex++;
                            Invoke(new Action(() =>
                            {
                                lblLogRestoreShopify.Text = string.Format("Total Removed: {0} - Scanning: '{1}' - Limit Page: {2} - Page Index: {3}", totalProductDeleted, store.Url, pageLimit, pageIndex);
                                lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                            }));
                        }
                    });
                    rtbLogTab4.Focus();
                }
                catch (Exception ex)
                {
                    rtbLogTab4.Focus();
                    rtbLogTab4.AppendText(Environment.NewLine);
                    rtbLogTab4.AppendText($"Error: {ex.Message} - {store.Url}");
                }
            }
            MessageBox.Show($"There are {totalProductDeleted} defective products have been removed.", "The delete process is complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            TurnOnButtons();
        }

        private async void btnProductCount_Click(object sender, EventArgs e)
        {
            TurnOffButtons();
            int totalProduct = 0;
            foreach (var store in listStore)
            {
                try
                {
                    rtbLogTab4.Focus();
                    rtbLogTab4.AppendText(Environment.NewLine);
                    rtbLogTab4.AppendText($"******Scanning store: '{store.Url}'!******");
                    string myShopifyUrl = store.Url;
                    string shopAccessToken = store.Pass;
                    var service = new ProductService(myShopifyUrl, shopAccessToken);
                    service.SetExecutionPolicy(new SmartRetryExecutionPolicy());
                    await Task.Run(async () =>
                    {
                        var totalProductByStore = await service.CountAsync(new ProductCountFilter
                        {
                            //PublishedStatus = "published",
                            //ProductType = "Framed Matte Canvas"
                        });
                        totalProduct += totalProductByStore;
                        Invoke(new Action(() =>
                        {
                            rtbLogTab4.Focus();
                            rtbLogTab4.AppendText(Environment.NewLine);
                            rtbLogTab4.AppendText(string.Format("Scanned: '{0}', and there are {1} products in the store.", store.Url, totalProductByStore));
                            lblLogRestoreShopify.Text = string.Format("Total Product: {0} - Scanned: '{1}', and there are {2} products in the store.", totalProduct, store.Url, totalProductByStore);
                            lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                        }));
                    });
                    rtbLogTab4.Focus();
                }
                catch (Exception ex)
                {
                    rtbLogTab4.Focus();
                    rtbLogTab4.AppendText(Environment.NewLine);
                    rtbLogTab4.AppendText($"Error: {ex.Message} - {store.Url}");
                }
            }
            lblLogRestoreShopify.Text = $"Completed! There are a total of {totalProduct} products found.";
            lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
            MessageBox.Show($"There are a total of {totalProduct} products found.", "Complete the product counting process!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            TurnOnButtons();
        }

        private void TurnOffButtons()
        {
            btnDeleteProductError.Enabled = false;
            btnProductCount.Enabled = false;
            btnBackupShopify.Enabled = false;
            btnRestoreShopify.Enabled = false;
            btnPrepare.Enabled = false;
            btnSynchronization.Enabled = false;
            btnExportShopbaseCSV.Enabled = false;
            btnRestoreToShopbaseCSV.Enabled = false;
            btnPublishAll.Enabled = false;
            btnExportBusinessData.Enabled = false;
        }

        private void TurnOnButtons()
        {
            btnDeleteProductError.Enabled = true;
            btnProductCount.Enabled = true;
            btnBackupShopify.Enabled = true;
            btnRestoreShopify.Enabled = true;
            btnPrepare.Enabled = true;
            btnSynchronization.Enabled = true;
            btnExportShopbaseCSV.Enabled = true;
            btnRestoreToShopbaseCSV.Enabled = true;
            btnPublishAll.Enabled = true;
            btnExportBusinessData.Enabled = true;
        }

        private async void btnSynchronization_Click(object sender, EventArgs e)
        {
            TurnOffButtons();
            int totalProductPublished = 0;
            foreach (var store in listStore)
            {
                try
                {
                    rtbLogTab4.Focus();
                    rtbLogTab4.AppendText(Environment.NewLine);
                    rtbLogTab4.AppendText($"******Scanning store: '{store.Url}'!******");
                    string myShopifyUrl = store.Url;
                    string shopAccessToken = store.Pass;
                    var service = new ProductService(myShopifyUrl, shopAccessToken);
                    service.SetExecutionPolicy(new SmartRetryExecutionPolicy());
                    int productCount = 0;
                    await Task.Run(async () =>
                    {
                        int pageLimit = 50;
                        int pageIndex = 1;
                        var filter = new ProductListFilter
                        {
                            Limit = pageLimit
                        };
                        var products = await service.ListAsync(filter);
                        while (true)
                        {
                            if (products.Items.Count() == 0)
                                break;
                            else
                            {
                                foreach (var product in products.Items)
                                {
                                    productCount++;
                                    totalProductPublished++;
                                    PublishProduct(product, "", "");
                                    Invoke(new Action(() =>
                                    {
                                        rtbLogTab4.Focus();
                                        rtbLogTab4.AppendText(Environment.NewLine);
                                        rtbLogTab4.AppendText($"Published productId: {product.Title}: {productCount} / {totalProductPublished} - {store.Url}");
                                        lblLogRestoreShopify.Text = string.Format("Total Removed: {0} - Scanning: '{1}' - Limit Page: {2} - Page Index: {3}", totalProductPublished, store.Url, pageLimit, pageIndex);
                                        lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                                    }));
                                }
                            }
                            if (!products.HasNextPage)
                            {
                                Invoke(new Action(() =>
                                {
                                    rtbLogTab4.Focus();
                                    rtbLogTab4.AppendText(Environment.NewLine);
                                    rtbLogTab4.AppendText($"******Scanned store: '{store.Url}' and {productCount} product in store removed!******");
                                }));
                                break;
                            }
                            products = await service.ListAsync(products.GetNextPageFilter());
                            pageIndex++;
                            Invoke(new Action(() =>
                            {
                                lblLogRestoreShopify.Text = string.Format("Total Published: {0} - Scanning: '{1}' - Limit Page: {2} - Page Index: {3}", totalProductPublished, store.Url, pageLimit, pageIndex);
                                lblLogLastTimeSuccessRestore.Text = string.Format("Latest Success - {0}", DateTime.Now.ToLongTimeString());
                            }));
                        }
                    });
                    rtbLogTab4.Focus();
                }
                catch (Exception ex)
                {
                    rtbLogTab4.Focus();
                    rtbLogTab4.AppendText(Environment.NewLine);
                    rtbLogTab4.AppendText($"Error: {ex.Message} - {store.Url}");
                }
            }
            MessageBox.Show($"There are {totalProductPublished} defective products have been published.", "The publish process is complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            TurnOnButtons();
        }

        public static async void PublishProduct(Product product, string adminSpfUrl, string adminSpfPassword)
        {
            var productService = new ProductService(adminSpfUrl, adminSpfPassword);
            productService.SetExecutionPolicy(new SmartRetryExecutionPolicy());
            var filter = new ProductListFilter()
            {
                Title = product.Title
            };
            var allProduct = await productService.ListAsync(filter);
            if (allProduct.Items.Any())
            {
                await productService.PublishAsync(allProduct.Items.FirstOrDefault().Id.Value);
            }

        }
        private ProductVariant SetNewVariant(ProductVariant variant, int option, string valueSize, string valuesPack)
        {
            try
            {
                var newVariant = new ProductVariant();
                switch (option)
                {
                    case 1:
                        newVariant.Option1 = valueSize;
                        break;
                    case 2:
                        newVariant.Option1 = valueSize;
                        newVariant.Option2 = valuesPack;
                        break;
                    case 3:
                        newVariant.Option1 = variant.Option1;
                        newVariant.Option2 = valueSize;
                        newVariant.Option3 = valuesPack;
                        break;
                    default: break;
                }
                newVariant.Position = variant.Position;
                newVariant.Barcode = variant.Barcode;
                newVariant.SKU = variant.SKU + "-" + valuesPack;
                newVariant.InventoryQuantity = 9999999;
                newVariant.FulfillmentService = "Manual";
                newVariant.Grams = variant.Grams;
                newVariant.ImageId = variant.ImageId;
                newVariant.InventoryPolicy = variant.InventoryPolicy;
                switch (valuesPack)
                {
                    case "One Item":
                        newVariant.Price = variant.Price;
                        break;
                    case "Pack 3":
                        newVariant.Price = variant.Price * 3 - 9;
                        break;
                    case "Pack 5":
                        newVariant.Price = variant.Price * 5 - 20;
                        break;
                    case "Pack 10":
                        newVariant.Price = variant.Price * 10 - 50;
                        break;
                    default: break;
                }
                newVariant.CompareAtPrice = variant.CompareAtPrice;
                newVariant.Title = variant.Title;
                newVariant.Weight = variant.Weight;
                newVariant.WeightUnit = variant.WeightUnit;
                return newVariant;
            }
            catch (Exception ex)
            {

            }
            return null;
        }


        private async void btnDeleteInTab4_Click(object sender, EventArgs e)
        {
            if (storeSelectedTab5 > 0)
            {
                btnUpdateInTab5.Enabled = false;
                var store = db.Shopbase_Stores.FirstOrDefault(p => p.Id == storeSelectedTab5);
                if (store == null)
                {
                    MessageBox.Show("Can not get store!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string myShopifyUrl = store.AdminUrl;
                string shopAccessToken = store.Password;
                string folderBackupShopify = string.Empty;
                var productService = new ShopbaseService.ProductService(store.AdminUrl, store.Password, store.ApiKey);
                productService.SetExecutionPolicy(new SmartRetryExecutionPolicy());
                int totatUpdated = 0;
                foreach (string line in rtbListTitle.Lines)
                {
                    if (string.IsNullOrEmpty(line))
                        continue;
                    #region Update Title by New Title
                    //string[] args = line.Split('\t', '|');
                    //args = args.Where(p => !string.IsNullOrEmpty(p)).ToArray();
                    //if (args.Count() == 2)
                    //{
                    //    var filter = new ProductListFilter()
                    //    {
                    //        Title = args[0]
                    //    };
                    //    var allProduct = await productService.ListAsync(filter);
                    //    foreach (var product in allProduct.Items.Where(p=>p.Title.Equals(filter.Title)))
                    //    {
                    //        //listProductShopifyPrepare.Add(product);
                    //        product.Title = args[1];
                    //        await productService.UpdateAsync(product.Id.Value, product);
                    //        //await productService.UnpublishAsync(product.Id.Value);
                    //        totatRemoved++;
                    //        rtbTab5.Focus();
                    //        rtbTab5.AppendText(Environment.NewLine);
                    //        rtbTab5.AppendText($"-{totatRemoved} Unpublish: '{product.Title}'");
                    //    }
                    //}
                    #endregion
                    #region Product By Title
                    var filter = new ProductListFilter()
                    {
                        Title = line.Replace("\t", "")
                    };
                    var allProduct = await productService.ListAsync(filter);

                    foreach (var product in allProduct.Items.Where(p => p.Title.Equals(filter.Title)))
                    {
                        listProductShopifyPrepare.Add(product);
                        //await productService.DeleteAsync(product.Id.Value);
                        //await productService.UnpublishAsync(product.Id.Value);
                        if (product.Tags.Contains("has-sales"))
                            continue;
                        product.Tags += ",has-sales";
                        try
                        {
                            //if (product.Variants.First().InventoryQuantity == 0)
                            //    continue;
                            //if (product.ProductType.Contains("Good"))
                            //    continue;
                            //foreach (var variant in product.Variants)
                            //{
                            //    variant.InventoryPolicy = "deny";
                            //    variant.FulfillmentService = "shopbase";
                            //    variant.InventoryManagement = "shopbase";
                            //    variant.InventoryQuantity = 0;
                            //}
                            //product.Tags += ",best-selling";
                            //product.ProductType += " - Good";
                            var productUpdated = await productService.UpdateAsync(product.Id.Value, product);
                        }
                        catch (Exception ex)
                        {
                            rtbTab5.Focus();
                            rtbTab5.AppendText(Environment.NewLine);
                            rtbTab5.AppendText($"-{totatUpdated} Error updte: '{product.Title}'");
                            rtbTab5.AppendText(Environment.NewLine);
                            rtbTab5.AppendText($"-{totatUpdated} Error: '{ex.Message}'");
                            continue;
                        }
                        totatUpdated++;
                        rtbTab5.Focus();
                        rtbTab5.AppendText(Environment.NewLine);
                        rtbTab5.AppendText($"-{totatUpdated} Updated: '{product.Title}'");
                    }
                    #endregion
                }
                MessageBox.Show("Complete!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("You need select a store!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            btnUpdateInTab5.Enabled = true;
        }

        private void btnExportShopbaseCSV_Click(object sender, EventArgs e)
        {
            btnExportShopbaseCSV.Enabled = false;
            try
            {
                ExportProductToShopbase();
            }
            catch (Exception ex)
            {
                // do nothing
                MessageBox.Show(ex.Message);
            }
            btnExportShopbaseCSV.Enabled = true;
            MessageBox.Show("Finish Export Shopbase CSV File!", "Complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private async void ExportProductToShopbase(string defaultName = "FileBackup", List<ShopifyCustomService.Product> productList = null)
        {
            string FolderShopbaseExp = "ShopbaseExportProducts";
            if (productList == null)
            {
                productList = listProductPrepare;
            }
            await Task.Run(() =>
            {
                if (!Directory.Exists(FolderShopbaseExp))
                {
                    Directory.CreateDirectory(FolderShopbaseExp);
                }
                string fileNameResult = string.Format("{0:dddd, MMMM d, yyyy, HH_mm_ss}", DateTime.Now);
                #region file export
                using (TextWriter writer = File.CreateText(string.Format("{0}/{1}_{2}.csv", FolderShopbaseExp, defaultName, fileNameResult)))
                {
                    var csv = new CsvWriter(writer);
                    csv.Configuration.HasHeaderRecord = true;

                    // print header
                    csv.WriteField("Handle");
                    csv.WriteField("Title");
                    csv.WriteField("Body (Html)");
                    csv.WriteField("Vendor");
                    csv.WriteField("Type");
                    csv.WriteField("Tags");
                    csv.WriteField("Published");
                    csv.WriteField("Option1 Name");
                    csv.WriteField("Option1 Value");
                    csv.WriteField("Option2 Name");
                    csv.WriteField("Option2 Value");
                    csv.WriteField("Option3 Name");
                    csv.WriteField("Option3 Value");
                    csv.WriteField("Variant SKU");
                    csv.WriteField("Variant Grams");
                    csv.WriteField("Variant Inventory Tracker");
                    csv.WriteField("Variant Inventory Qty");
                    csv.WriteField("Variant Inventory Policy");
                    csv.WriteField("Variant Fulfillment Service");
                    csv.WriteField("Variant Price");
                    csv.WriteField("Variant Compare At Price");
                    csv.WriteField("Variant Requires Shipping");
                    csv.WriteField("Variant Taxable");
                    csv.WriteField("Variant Barcode");
                    csv.WriteField("Image Src");
                    csv.WriteField("Image Position");
                    csv.WriteField("Image Alt Text");
                    csv.WriteField("Gift Card");
                    csv.WriteField("SEO Title");
                    csv.WriteField("SEO Description");
                    csv.WriteField("Google Shopping / Google Product Category");
                    csv.WriteField("Google Shopping / Gender");
                    csv.WriteField("Google Shopping / Age Group");
                    csv.WriteField("Google Shopping / MPN");
                    csv.WriteField("Google Shopping / AdWords Grouping");
                    csv.WriteField("Google Shopping / AdWords Labels");
                    csv.WriteField("Google Shopping / Condition");
                    csv.WriteField("Google Shopping / Custom Product");
                    csv.WriteField("Google Shopping / Custom Label 0");
                    csv.WriteField("Google Shopping / Custom Label 1");
                    csv.WriteField("Google Shopping / Custom Label 2");
                    csv.WriteField("Google Shopping / Custom Label 3");
                    csv.WriteField("Google Shopping / Custom Label 4");
                    csv.WriteField("Variant Image");
                    csv.WriteField("Variant Weight Unit");
                    csv.WriteField("Variant Tax Code");
                    csv.WriteField("Cost Per Item");
                    csv.WriteField("Available On Listing Pages");
                    csv.WriteField("Available On Sitemap Files");
                    csv.NextRecord();

                    foreach (var product in productList)
                    {
                        //product.BodyHtml = "<p>a</p>";
                        bool isFirstRows = true;
                        int indexImagePosition = 0;
                        int curentImagePosition = 0;
                        var imagesPostions = product.Images;
                        foreach (var variant in product.Variants)
                        {

                            //Handle
                            csv.WriteField(product.Handle);

                            //Title
                            if (isFirstRows)
                                csv.WriteField(product.Title);
                            else
                                csv.WriteField(string.Empty);

                            //Body (HTML)
                            if (isFirstRows)
                                csv.WriteField(product.BodyHtml);
                            else
                                csv.WriteField(string.Empty);

                            //Vender
                            if (isFirstRows)
                                csv.WriteField(product.Vendor);
                            else
                                csv.WriteField(string.Empty);

                            //Type
                            if (isFirstRows)
                                csv.WriteField(product.ProductType);
                            else
                                csv.WriteField(string.Empty);

                            //Tags
                            if (isFirstRows)
                                csv.WriteField(product.Tags);
                            else
                                csv.WriteField(string.Empty);

                            //Published
                            if (isFirstRows)
                                csv.WriteField("FALSE");
                            else
                                csv.WriteField(string.Empty);

                            // Option1 Name
                            if (isFirstRows)
                            {
                                var option1 = product.Options.Where(p => p.Values.Contains(variant.Option1)).FirstOrDefault();
                                if (option1 != null && option1.Values.Any())
                                    csv.WriteField(option1.Name);
                                else
                                    csv.WriteField(string.Empty);
                            }
                            else
                                csv.WriteField(string.Empty);

                            // Option1 Value
                            csv.WriteField(variant.Option1);

                            // Option2 Name
                            if (isFirstRows)
                            {
                                var option2 = product.Options.Where(p => p.Values.Contains(variant.Option2)).FirstOrDefault();
                                if (option2 != null && option2.Values.Any())
                                    csv.WriteField(option2.Name);
                                else
                                    csv.WriteField(string.Empty);
                            }
                            else
                                csv.WriteField(string.Empty);

                            // Option2 Value
                            csv.WriteField(variant.Option2);

                            // Option3 Name
                            if (isFirstRows)
                            {
                                var option3 = product.Options.Where(p => p.Values.Contains(variant.Option3)).FirstOrDefault();
                                if (option3 != null && option3.Values.Any())
                                    csv.WriteField(option3.Name);
                                else
                                    csv.WriteField(string.Empty);
                            }
                            else
                                csv.WriteField(string.Empty);

                            // Option3 Value
                            csv.WriteField(variant.Option3);

                            // Variant SKU
                            csv.WriteField(variant.SKU);

                            // Variant Grams
                            csv.WriteField(0);

                            // Variant Inventory Tracker
                            csv.WriteField(string.Empty);

                            // Variant Inventory Qty
                            if (variant.InventoryQuantity.HasValue)
                                csv.WriteField(999999);
                            else
                                csv.WriteField(string.Empty);

                            // Variant Inventory Policy
                            csv.WriteField("deny");

                            // Variant Fulfillment Service
                            csv.WriteField("manual");

                            // Variant Price
                            csv.WriteField(variant.Price);

                            // Variant Compare At Price
                            csv.WriteField(0);

                            // Variant Requires Shipping
                            csv.WriteField("TRUE");

                            // Variant Taxable
                            csv.WriteField("TRUE");

                            // Variant Barcode
                            csv.WriteField(variant.Barcode);

                            // Image Src
                            var src = imagesPostions.FirstOrDefault();
                            if (src != null)
                            {
                                csv.WriteField(src.Src);
                                imagesPostions = imagesPostions.Where(p => p.Id != src.Id);
                                curentImagePosition++;
                            }
                            else
                                csv.WriteField(string.Empty);

                            //if (isFirstColor)
                            //{
                            //    csv.WriteField(colorVariant.ImageUrl);
                            //    isFirstColor = false;
                            //}else
                            //    csv.WriteField(string.Empty);


                            // Image Position
                            if (curentImagePosition > indexImagePosition)
                            {
                                csv.WriteField(curentImagePosition);
                            }
                            else
                                csv.WriteField(string.Empty);

                            // Image Alt Text
                            csv.WriteField(string.Empty);

                            // Gift Card
                            csv.WriteField(string.Empty);

                            // SEO Title
                            csv.WriteField(string.Empty);

                            // SEO Description
                            csv.WriteField(string.Empty);

                            // Google Shopping / Google Product Category
                            csv.WriteField("");

                            // Google Shopping / Gender
                            csv.WriteField("");

                            // Google Shopping / Age Group
                            csv.WriteField("");

                            // Google Shopping / MPN
                            csv.WriteField("");

                            // Google Shopping / AdWords Grouping
                            csv.WriteField("");

                            // Google Shopping / AdWords Labels
                            csv.WriteField("");

                            // Google Shopping / Condition
                            csv.WriteField("");

                            // Google Shopping / Custom Product
                            csv.WriteField("");

                            // Google Shopping / Custom Label 0
                            csv.WriteField("");

                            // Google Shopping / Custom Label 1
                            csv.WriteField("");

                            // Google Shopping / Custom Label 2
                            csv.WriteField("");

                            // Google Shopping / Custom Label 3
                            csv.WriteField("");

                            // Google Shopping / Custom Label 4
                            csv.WriteField("");

                            // Variant Image
                            if (variant.ImageId.HasValue)
                            {
                                var image = product.Images.FirstOrDefault(p => p.Id.Value == variant.ImageId.Value);
                                if (image != null)
                                    csv.WriteField(image.Src);
                                else
                                    csv.WriteField(string.Empty);
                            }
                            else
                                csv.WriteField("");

                            // Variant Weight Unit
                            csv.WriteField("kg");

                            // Variant Tax Code
                            csv.WriteField("");

                            //Cost Per Item
                            csv.WriteField(string.Empty);

                            //Available On Listing Pages
                            if (isFirstRows)
                                csv.WriteField("TRUE");
                            else
                                csv.WriteField(string.Empty);

                            //Available On Sitemap Files
                            if (isFirstRows)
                                csv.WriteField("TRUE");
                            else
                                csv.WriteField(string.Empty);
                            csv.NextRecord();
                            indexImagePosition++;
                            isFirstRows = false;
                        }
                        if (product.Images.Count() > product.Variants.Count())
                        {
                            var listMissingImages = product.Images.Skip(product.Variants.Count());
                            foreach (var imageMissing in listMissingImages)
                            {
                                //Handle
                                csv.WriteField(product.Handle);

                                //Title
                                csv.WriteField(string.Empty);

                                //Body (HTML)
                                csv.WriteField(string.Empty);

                                //Vender
                                csv.WriteField(string.Empty);

                                //Type
                                csv.WriteField(string.Empty);

                                //Tags
                                csv.WriteField(string.Empty);

                                //Published
                                csv.WriteField(string.Empty);

                                // Option1 Name
                                csv.WriteField(string.Empty);

                                // Option1 Value
                                csv.WriteField(string.Empty);

                                // Option2 Name
                                csv.WriteField(string.Empty);

                                // Option2 Value
                                csv.WriteField(string.Empty);

                                // Option3 Name
                                csv.WriteField(string.Empty);

                                // Option3 Value
                                csv.WriteField(string.Empty);

                                // Variant SKU
                                csv.WriteField(string.Empty);

                                // Variant Grams
                                csv.WriteField(string.Empty);

                                // Variant Inventory Tracker
                                csv.WriteField(string.Empty);

                                // Variant Inventory Qty
                                csv.WriteField(string.Empty);

                                // Variant Inventory Policy
                                csv.WriteField(string.Empty);

                                // Variant Fulfillment Service
                                csv.WriteField(string.Empty);

                                // Variant Price
                                csv.WriteField(string.Empty);

                                // Variant Compare At Price
                                csv.WriteField(string.Empty);

                                // Variant Requires Shipping
                                csv.WriteField(string.Empty);

                                // Variant Taxable
                                csv.WriteField(string.Empty);

                                // Variant Barcode
                                csv.WriteField(string.Empty);

                                // Image Src
                                csv.WriteField(imageMissing.Src);
                                curentImagePosition++;

                                // Image Position
                                csv.WriteField(curentImagePosition);

                                // Image Alt Text
                                csv.WriteField(string.Empty);

                                // Gift Card
                                csv.WriteField(string.Empty);

                                // SEO Title
                                csv.WriteField(string.Empty);

                                // SEO Description
                                csv.WriteField(string.Empty);

                                // Google Shopping / Google Product Category
                                csv.WriteField(string.Empty);

                                // Google Shopping / Gender
                                csv.WriteField(string.Empty);

                                // Google Shopping / Age Group
                                csv.WriteField(string.Empty);

                                // Google Shopping / MPN
                                csv.WriteField(string.Empty);

                                // Google Shopping / AdWords Grouping
                                csv.WriteField(string.Empty);

                                // Google Shopping / AdWords Labels
                                csv.WriteField(string.Empty);

                                // Google Shopping / Condition
                                csv.WriteField(string.Empty);

                                // Google Shopping / Custom Product
                                csv.WriteField(string.Empty);

                                // Google Shopping / Custom Label 0
                                csv.WriteField(string.Empty);

                                // Google Shopping / Custom Label 1
                                csv.WriteField(string.Empty);

                                // Google Shopping / Custom Label 2
                                csv.WriteField(string.Empty);

                                // Google Shopping / Custom Label 3
                                csv.WriteField(string.Empty);

                                // Google Shopping / Custom Label 4
                                csv.WriteField(string.Empty);

                                // Variant Image
                                csv.WriteField(string.Empty);

                                // Variant Weight Unit
                                csv.WriteField(string.Empty);

                                // Variant Tax Code
                                csv.WriteField(string.Empty);

                                //Cost Per Item
                                csv.WriteField(string.Empty);

                                //Available On Listing Pages
                                csv.WriteField(string.Empty);

                                //Available On Sitemap Files
                                csv.WriteField(string.Empty);
                                csv.NextRecord();
                            }
                        }
                    }

                }
                #endregion
                #region custom export
                //using (TextWriter writer = File.CreateText(string.Format("{0}/{1}_{2}.csv", FolderShopbaseExp, defaultName, fileNameResult)))
                //{
                //    var csv = new CsvWriter(writer);
                //    csv.Configuration.HasHeaderRecord = true;

                //    // print header
                //    csv.WriteField("Handle");
                //    csv.WriteField("Title");
                //    csv.NextRecord();

                //    foreach (var product in productList)
                //    {

                //        //Handle
                //        csv.WriteField("https://gobicyclegear.com/products/" + product.Handle);

                //        //Title
                //        csv.WriteField(product.Title);

                //        csv.NextRecord();
                //    }
                //}
                #endregion
            });
        }
        private async void btnExportTab4_Click(object sender, EventArgs e)
        {

            TurnOffButtons();
            btnExportShopbaseCSV.Hide();
            listProductPrepare = new List<ShopifyCustomService.Product>();
            string folderBackup = "shopify-backup/";
            folderBackup += string.Format("{0}-{1}-{2}-{3}-{4}-{5}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            if (!string.IsNullOrEmpty(txtTitle.Text))
            {
                folderBackup += "-" + txtTitle.Text;
            }
            if (!string.IsNullOrEmpty(txtProductType.Text))
            {
                folderBackup += "-" + txtProductType.Text;
            }
            if (Directory.Exists(folderBackup))
            {
                new System.IO.DirectoryInfo(folderBackup).Delete(true);
            }
            Directory.CreateDirectory(folderBackup);
            string jsonResult = JsonConvert.SerializeObject(listProductShopifyPrepare);
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(string.Format("{0}/product-filter-by-title-{1}-items.txt", folderBackup, listProductShopifyPrepare.Count), false))
            {
                file.WriteLine(jsonResult);
            }
        }

        private async void btnRestoreToShopbaseCSV_Click(object sender, EventArgs e)
        {
            TurnOffButtons();
            int packageSize = 7000;
            //folderBackupShopify = @"C:\Workspace\tool-import-product-nop\EggStoreImportProduct\bin\Debug\shopify-backup\2019-9-27";
            string folderBackupShopify = string.Empty;
            using (var fldrDlg = new FolderBrowserDialog())
            {
                if (fldrDlg.ShowDialog() == DialogResult.OK)
                {
                    folderBackupShopify = fldrDlg.SelectedPath;
                }
            }
            lblFolderRestore.Text = folderBackupShopify;
            listProductPrepare = new List<ShopifyCustomService.Product>();
            await Task.Run(() =>
            {
                if (!string.IsNullOrEmpty(folderBackupShopify))
                {
                    DirectoryInfo di = new DirectoryInfo(folderBackupShopify);
                    var allFilesBackup = di.GetFiles();
                    int indexFile = 0;
                    foreach (var fileBackup in allFilesBackup)
                    {
                        indexFile++;
                        string contentJson = string.Empty;
                        var fileStream = new FileStream(fileBackup.FullName, FileMode.Open, FileAccess.Read);
                        using (var streamReader = new StreamReader(fileStream))
                        {
                            contentJson = streamReader.ReadToEnd();
                        }
                        Invoke(new Action(() =>
                        {
                            rtbLogTab4.Focus();
                            rtbLogTab4.AppendText(Environment.NewLine);
                            rtbLogTab4.AppendText($"-Preparing file: {fileBackup.FullName}");
                        }));
                        var productsInFiles = JsonConvert.DeserializeObject<List<ShopifyCustomService.Product>>(contentJson);
                        listProductPrepare.AddRange(productsInFiles);
                        //listProductPrepare = listProductPrepare.Where(p => p.Title.Contains("Blanket")).ToList();
                        if (listProductPrepare.Count >= packageSize || indexFile == allFilesBackup.Count())
                        {
                            ExportProductToShopbase($"Import-File-{indexFile}", listProductPrepare);
                            listProductPrepare = new List<ShopifyCustomService.Product>();
                        }
                    }
                }
            });
            db.SaveChanges();
            TurnOnButtons();
        }

        private async void btnImportADS_Click(object sender, EventArgs e)
        {
            storeSelectedTab6 = 1114;
            if (storeSelectedTab6 > 0)
            {
                var store = dbW9.Stores.FirstOrDefault(p => p.Id == storeSelectedTab6);
                if (store == null)
                {
                    MessageBox.Show("Can not get store!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                foreach (string line in rtbADS.Lines)
                {
                    if (string.IsNullOrEmpty(line))
                        continue;

                    string[] args = line.Split('\t', ' ');
                    args = args.Where(p => !string.IsNullOrEmpty(p)).ToArray();
                    if (args.Count() == 2)
                    {
                        var time = args[0];
                        var amountVND = args[1];
                        var note = "GG ADS đã tính";
                        var currentUserLogin = dbW9.AspNetUsers.Where(p => p.Email.Equals("daiphan1996@gmail.com")).FirstOrDefault();
                        if (currentUserLogin == null)
                            throw new Exception("Current user did not login to the application!");
                        amountVND = amountVND.Replace(",00", "");
                        amountVND = amountVND.Replace(".", "");
                        DateTime date = Convert.ToDateTime(time);
                        // client timezone
                        var amount = int.Parse(amountVND) / (float)22900;
                        var _timeZone = GetUserTimezone(currentUserLogin);
                        var creationTime = ConvertToFilterStartDate(date);
                        creationTime = ConvertToUtcTime(creationTime, _timeZone);
                        if (amount <= 0)
                        {
                            MessageBox.Show("Ad spending amount must be more than Zero!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            continue;
                        }
                        if (creationTime > DateTime.UtcNow)
                        {
                            MessageBox.Show("Date must be less than UTC time now", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            continue;
                        }

                        var adSpending = new StoreAdSpending
                        {
                            Amount = (decimal)Math.Round(amount, 2),
                            Note = note,
                            StoreId = store.Id,
                            CreatedBy = currentUserLogin.Email,
                            CreatedOnUtc = creationTime
                        };
                        dbW9.StoreAdSpendings.Add(adSpending);
                        dbW9.SaveChanges();
                    }

                }
                MessageBox.Show("Complete!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("You need select a store!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public static TimeZoneInfo GetUserTimezone(AspNetUser user)
        {
            string timezoneId = !string.IsNullOrEmpty(user.CurrentTimezoneId) ? user.CurrentTimezoneId : TimeZoneInfo.Local.Id;
            var allTimezones = TimeZoneInfo.GetSystemTimeZones().GroupBy(p => p.BaseUtcOffset).Select(p => p.FirstOrDefault()).ToList();
            var userTimezone = allTimezones.FirstOrDefault(p => p.Id.Equals(timezoneId));
            if (userTimezone == null)
                userTimezone = allTimezones.Where(p => p.BaseUtcOffset.Hours == TimeZoneInfo.Local.BaseUtcOffset.Hours).FirstOrDefault();
            return userTimezone;
        }
        public static DateTime ConvertToFilterStartDate(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day);
        }
        public static DateTime ConvertToUtcTime(DateTime dt, TimeZoneInfo sourceTimeZone)
        {
            if (sourceTimeZone.IsInvalidTime(dt))
            {
                //could not convert
                return dt;
            }

            return TimeZoneInfo.ConvertTimeToUtc(dt, sourceTimeZone);
        }

        private async void btnExportBusinessData_Click(object sender, EventArgs e)
        {
            TurnOffButtons();
            if (storeSelected > 0)
            {
                var store = db.Shopbase_Stores.FirstOrDefault(p => p.Id == storeSelected);
                if (store == null)
                {
                    MessageBox.Show("Can not get store!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string myShopifyUrl = store.AdminUrl;
                string shopAccessToken = store.Password;
                string folderBackupShopify = string.Empty;
                var service = new ProductService(myShopifyUrl, shopAccessToken);
                service.SetExecutionPolicy(new SmartRetryExecutionPolicy());

                TurnOnButtons();
                string FolderShopbaseExp = "ShopbaseExportBusinessData";

                await Task.Run(() =>
                {
                    if (!Directory.Exists(FolderShopbaseExp))
                    {
                        Directory.CreateDirectory(FolderShopbaseExp);
                    }
                    string fileNameResult = string.Format("{0:dddd, MMMM d, yyyy, HH_mm_ss}", DateTime.Now);
                    using (TextWriter writer = File.CreateText(string.Format("{0}/{1}_{2}.csv", FolderShopbaseExp, "BusinessData", fileNameResult)))
                    {
                        var csv = new CsvWriter(writer);
                        csv.Configuration.HasHeaderRecord = true;

                        // print header
                        csv.WriteField("ID");
                        csv.WriteField("ID2");
                        csv.WriteField("Item title");
                        csv.WriteField("Final URL");
                        csv.WriteField("Image URL");
                        csv.WriteField("Item subtitle");
                        csv.WriteField("Item description");
                        csv.WriteField("Item category");
                        csv.WriteField("Price");
                        csv.WriteField("Sale price");
                        csv.WriteField("Contextual keywords");
                        csv.WriteField("Item address");
                        csv.WriteField("Tracking template");
                        csv.WriteField("Custom parameter");
                        csv.WriteField("Final mobile URL");
                        csv.WriteField("Android app link");
                        csv.WriteField("iOS app link");
                        csv.WriteField("iOS app store ID");
                        csv.NextRecord();

                        foreach (var product in listProductPrepare)
                        {
                            if (!product.Images.Any() || !product.Variants.Any())
                                continue;
                            //ID
                            csv.WriteField(product.Id.Value);

                            //ID2
                            csv.WriteField(product.ProductType);

                            //Item title
                            csv.WriteField(product.Title);

                            //Final URL
                            csv.WriteField(product.Vendor);

                            //Final URL
                            csv.WriteField("https://www.gameodea.com/products/" + product.Handle);

                            //Image URL
                            csv.WriteField(product.Images.FirstOrDefault().Src);

                            //Item subtitle
                            csv.WriteField(product.Title);

                            //Item description
                            csv.WriteField(Common.HtmlToPlainText(product.BodyHtml));

                            //Item category
                            csv.WriteField(7230);

                            //Price
                            csv.WriteField(product.Variants.FirstOrDefault().Price);

                            //Sale price
                            csv.WriteField(string.Empty);

                            //Contextual keywords
                            csv.WriteField(string.Empty);

                            //Item address
                            csv.WriteField(string.Empty);

                            //Tracking template
                            csv.WriteField(string.Empty);

                            //Custom parameter
                            csv.WriteField(string.Empty);

                            //Final mobile URL
                            csv.WriteField(string.Empty);

                            //Android app link
                            csv.WriteField(string.Empty);

                            //iOS app link
                            csv.WriteField(string.Empty);

                            //iOS app store ID
                            csv.WriteField(string.Empty);

                            csv.NextRecord();
                        }
                    }
                });
            }
            else
            {
                MessageBox.Show("You need select a store!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private async void btnPublishAll_Click(object sender, EventArgs e)
        {
            TurnOffButtons();
            if (storeSelected > 0)
            {
                var store = db.Shopbase_Stores.FirstOrDefault(p => p.Id == storeSelected);
                if (store == null)
                {
                    MessageBox.Show("Can not get store!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                string myShopifyUrl = store.AdminUrl;
                string shopAccessToken = store.Password;
                string folderBackupShopify = string.Empty;
                var service = new ProductService(myShopifyUrl, shopAccessToken);
                service.SetExecutionPolicy(new SmartRetryExecutionPolicy());

                var serviceProductImage = new ProductImageService(myShopifyUrl, shopAccessToken);
                serviceProductImage.SetExecutionPolicy(new SmartRetryExecutionPolicy());

                var serviceProductVariant = new ProductVariantService(myShopifyUrl, shopAccessToken);
                serviceProductVariant.SetExecutionPolicy(new SmartRetryExecutionPolicy());

                var collectionService = new CustomCollectionService(myShopifyUrl, shopAccessToken);
                var defaultProduct = service.GetAsync(5850010747047);

                var allProduct = await service.ListAsync();
                while (true)
                {
                    //Update 
                    var allProductItems = allProduct.Items;
                    //totalSuccess += allProduct.Items.Count();
                    foreach (var product in allProductItems)
                    {
                        try
                        {
                            if (!product.Images.Any())
                            {
                                await service.DeleteAsync(product.Id.Value);
                                continue;
                            }
                            #region UpdateQuantity
                            if (product.Variants.Any(p => p.InventoryQuantity.HasValue && p.InventoryQuantity.Value < 10 || !p.InventoryQuantity.HasValue))
                            {
                                foreach (var variant in product.Variants)
                                {
                                    variant.FulfillmentService = "Manual";
                                    variant.InventoryQuantity = 999999;
                                }
                                await service.UpdateAsync(product.Id.Value, product);
                            }
                            #endregion
                            #region UpdateDes
                            //product.BodyHtml = defaultProduct.Result.BodyHtml;
                            //await service.UpdateAsync(product.Id.Value, product);
                            #endregion
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    if (!allProduct.HasNextPage)
                    {
                        break;
                    }
                    allProduct = await service.ListAsync(allProduct.GetNextPageFilter());
                }
            }
            else
            {
                MessageBox.Show("You need select a store!", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            TurnOnButtons();
        }

        private void rtbListRepository_TextChanged(object sender, EventArgs e)
        {

        }

        private async void btnBackupShopBase_Click(object sender, EventArgs e)
        {
            #region prepare store 
            var packageProducts = new List<ShopifyCustomService.Product>();
            foreach (var store in storeTomeeySelected)
            {
                #region condition
                await Task.Run(() =>
                {
                    var entities = new bonzaroEntities();
                    var importProducts = entities.ShopifyImportProducts.Where(p => p.CenterStoreId == store.Id).OrderByDescending(p => p.CreatedOnUtc).Take(2).ToList();
                    foreach (var prd in importProducts)
                    {
                        this.ProductTommeyToShopbase(prd).Wait();
                    }
                    //var preImportProduct = entities.PreImportProducts.Where(p => !p.Deleted).OrderBy(p => Guid.NewGuid()).FirstOrDefault();
                    //if(preImportProduct !=null)
                    //{
                    //    var parser = ProductStorage.ParsePreImportProduct(preImportProduct);
                    //}
                });
                #endregion


                rtbListRepository.Focus();
                if (rtbListRepository.Lines.Count() > 0)
                    rtbListRepository.AppendText(Environment.NewLine);
                rtbListRepository.AppendText($"{store.Url}   {store.ApiKey}  {store.ApiPassword} {store.Name.Replace(" ", "-")}");
            }
            #endregion
        }
        private async Task<ShopifyCustomService.Product> ProductTommeyToShopbase(ShopifyImportProduct importProduct)
        {
            try
            {
                if (importProduct == null)
                    throw new ArgumentNullException(nameof(ShopifyImportProduct));
                var preImportProduct = ProductStorage.ImportProductData(importProduct.Id);
                if (preImportProduct == null)
                    throw new Exception("No pre import data was found");
                var store = ProductStorage.ShopifyCenterStore(importProduct.CenterStoreId);
                if (store == null)
                    throw new Exception("Invalid store to import");
                //if (!store.IsShopbase())
                //    throw new Exception("Store must be based on SHOPBASE!!!");
                //store.Url = store.Url.TrimEnd('/');

                //parse base info
                var shopbaseProduct = new ShopifyCustomService.Product();
                shopbaseProduct.Title = preImportProduct.Title;
                shopbaseProduct.BodyHtml = $"<div>{(string.IsNullOrEmpty(preImportProduct.BodyHtml) ? preImportProduct.Title : preImportProduct.BodyHtml)}</div>";
                shopbaseProduct.Handle = string.IsNullOrEmpty(preImportProduct.Handle) ? Common.CreateSlug(preImportProduct.Title) : preImportProduct.Handle;
                shopbaseProduct.ProductType = preImportProduct.ProductType;
                shopbaseProduct.Tags = preImportProduct.Tags;
                shopbaseProduct.Id = preImportProduct.Id;
                //shopbaseProduct.Published = false;
                //shopbaseProduct.ProductAvailability = 1;

                //parse options
                shopbaseProduct.Options = new List<ShopifyCustomService.ProductOption>();
                var listOptions = new List<ShopifyCustomService.ProductOption>();
                foreach (var importOption in preImportProduct.ImportProductOptions)
                {
                    ShopifyCustomService.ProductOption option = new ShopifyCustomService.ProductOption();
                    option.Name = importOption.Name;
                    option.Position = importOption.Position;
                    option.ProductId = shopbaseProduct.Id;
                    option.Values = string.IsNullOrEmpty(importOption.Values) ? new List<string>() : importOption.Values.Split(',').ToList();
                    listOptions.Add(option);
                }
                shopbaseProduct.Options = listOptions;
                //parse variant
                shopbaseProduct.Variants = new List<ShopifyCustomService.ProductVariant>();
                var listVariants = new List<ShopifyCustomService.ProductVariant>();
                foreach (var importVariant in preImportProduct.ImportProductVariants)
                {
                    ShopifyCustomService.ProductVariant variant = new ShopifyCustomService.ProductVariant();
                    variant.Id = importVariant.Id;
                    variant.Title = importVariant.Title;
                    variant.Option1 = importVariant.Option1;
                    variant.Option2 = importVariant.Option2;
                    variant.Option3 = importVariant.Option3;
                    variant.Position = importVariant.Position;
                    variant.ImageId = importVariant.ImageId;
                    variant.SKU = importVariant.ImageId.HasValue ? importVariant.ImageId.Value.ToString() : null;
                    listVariants.Add(variant);
                }

                //this.LogMessage(message: $"Importing Product: {shopbaseProduct.Title} ", type: "info");

                var listImages = new List<ShopifyCustomService.ProductImage>();
                //create product image
                foreach (var importImage in preImportProduct.ImportProductImages)
                {
                    try
                    {
                        if (!importImage.ImageId.HasValue)
                            continue;
                        var imageStorage = ImportImageStorage.RetrieveImageStorage(importImage.ImageId.Value);
                        if (imageStorage == null)
                            continue;
                        if (string.IsNullOrEmpty(imageStorage.Source))
                            throw new Exception($"Failed to read image data {importImage.ImageId.Value}");
                        listImages.Add(new ShopifyCustomService.ProductImage
                        {
                            Id = importImage.Id,
                            Src = imageStorage.Source
                        });

                        //this.LogMessage(message: $"Created Image: {createImageResult.Src}", type: "info");

                    }
                    catch (Exception ex)
                    {
                        //log error
                        //this.LogMessage(message: $"Created Failed Image: {importImage.ImageId}", type: "error");
                    }
                }
                shopbaseProduct.Images = listImages;

                //mark import product as completed
                //importProduct.Status = (int)ShopifyImportStatus.Completed;
                //importProduct.ProductId = _newProduct.Id;
                //if (string.IsNullOrEmpty(importProduct.DefaultPicture))
                //   importProduct.DefaultPicture = defaultImage;
                //if (string.IsNullOrEmpty(importProduct.Url) || importProduct.Url == "local")
                //importProduct.Url = $"{store.Url}/products/{_newProduct.Handle}";
                //ProductStorage.UpdateImportProduct(importProduct);
                //log error
                //this.LogMessage(message: $"Imported Product:{_newProduct.Id}_{_newProduct.Handle}", type: "suceess");
                return shopbaseProduct;
            }
            catch (Exception ex)
            {
                //this.LogMessage(message: $"Importe Failed Product:{importProduct.Title} {ex.Message}", type: "error", newLine: 2);
            }
            return null;
        }
    }
}
