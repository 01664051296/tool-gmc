﻿namespace ToolCheckGMC
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.accountBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.checkBoxUpdateCollections = new System.Windows.Forms.CheckBox();
            this.checkBoxUpdateProducts = new System.Windows.Forms.CheckBox();
            this.checkBoxUpdatePage = new System.Windows.Forms.CheckBox();
            this.btnAddStore = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.rtbResponse = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddGMC = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.rtbLogTab3 = new System.Windows.Forms.RichTextBox();
            this.txtTitleFilter = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClone = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnBackupShopBase = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.checkedListStore = new System.Windows.Forms.CheckedListBox();
            this.btnPublishAll = new System.Windows.Forms.Button();
            this.btnExportBusinessData = new System.Windows.Forms.Button();
            this.btnRestoreToShopbaseCSV = new System.Windows.Forms.Button();
            this.btnExportShopbaseCSV = new System.Windows.Forms.Button();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSynchronization = new System.Windows.Forms.Button();
            this.btnProductCount = new System.Windows.Forms.Button();
            this.btnDeleteProductError = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lblLogLastTimeSuccessRestore = new System.Windows.Forms.Label();
            this.lblLogRestoreShopify = new System.Windows.Forms.Label();
            this.lblFolderRestore = new System.Windows.Forms.Label();
            this.btnRestoreShopify = new System.Windows.Forms.Button();
            this.btnBackupShopify = new System.Windows.Forms.Button();
            this.rtbLogTab4 = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rtbListRepository = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtProductType = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPrepare = new System.Windows.Forms.Button();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.btnExportTab4 = new System.Windows.Forms.Button();
            this.btnUpdateInTab5 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.rtbTab5 = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.rtbListTitle = new System.Windows.Forms.RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.btnImportADS = new System.Windows.Forms.Button();
            this.rtbADS = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.accountBindingSource)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // accountBindingSource
            // 
            this.accountBindingSource.DataMember = "Account";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.checkBoxUpdateCollections);
            this.tabPage2.Controls.Add(this.checkBoxUpdateProducts);
            this.tabPage2.Controls.Add(this.checkBoxUpdatePage);
            this.tabPage2.Controls.Add(this.btnAddStore);
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(957, 708);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Shop";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdateCollections
            // 
            this.checkBoxUpdateCollections.AutoSize = true;
            this.checkBoxUpdateCollections.Location = new System.Drawing.Point(294, 683);
            this.checkBoxUpdateCollections.Name = "checkBoxUpdateCollections";
            this.checkBoxUpdateCollections.Size = new System.Drawing.Size(115, 17);
            this.checkBoxUpdateCollections.TabIndex = 4;
            this.checkBoxUpdateCollections.Text = "Update Collections";
            this.checkBoxUpdateCollections.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdateProducts
            // 
            this.checkBoxUpdateProducts.AutoSize = true;
            this.checkBoxUpdateProducts.Location = new System.Drawing.Point(182, 684);
            this.checkBoxUpdateProducts.Name = "checkBoxUpdateProducts";
            this.checkBoxUpdateProducts.Size = new System.Drawing.Size(106, 17);
            this.checkBoxUpdateProducts.TabIndex = 3;
            this.checkBoxUpdateProducts.Text = "Update Products";
            this.checkBoxUpdateProducts.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdatePage
            // 
            this.checkBoxUpdatePage.AutoSize = true;
            this.checkBoxUpdatePage.Location = new System.Drawing.Point(86, 685);
            this.checkBoxUpdatePage.Name = "checkBoxUpdatePage";
            this.checkBoxUpdatePage.Size = new System.Drawing.Size(89, 17);
            this.checkBoxUpdatePage.TabIndex = 2;
            this.checkBoxUpdatePage.Text = "Update Page";
            this.checkBoxUpdatePage.UseVisualStyleBackColor = true;
            // 
            // btnAddStore
            // 
            this.btnAddStore.Location = new System.Drawing.Point(817, 679);
            this.btnAddStore.Name = "btnAddStore";
            this.btnAddStore.Size = new System.Drawing.Size(124, 23);
            this.btnAddStore.TabIndex = 1;
            this.btnAddStore.Text = "Add new store";
            this.btnAddStore.UseVisualStyleBackColor = true;
            this.btnAddStore.Click += new System.EventHandler(this.btnAddStore_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(6, 20);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(935, 653);
            this.dataGridView2.TabIndex = 0;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtSearch);
            this.tabPage1.Controls.Add(this.rtbResponse);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.btnAddGMC);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(957, 708);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Merchant Center Account";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(55, 13);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(219, 20);
            this.txtSearch.TabIndex = 5;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // rtbResponse
            // 
            this.rtbResponse.Location = new System.Drawing.Point(3, 350);
            this.rtbResponse.Name = "rtbResponse";
            this.rtbResponse.Size = new System.Drawing.Size(946, 337);
            this.rtbResponse.TabIndex = 2;
            this.rtbResponse.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Search";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 334);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Response";
            // 
            // btnAddGMC
            // 
            this.btnAddGMC.Location = new System.Drawing.Point(879, 10);
            this.btnAddGMC.Name = "btnAddGMC";
            this.btnAddGMC.Size = new System.Drawing.Size(70, 23);
            this.btnAddGMC.TabIndex = 1;
            this.btnAddGMC.Text = "Add New";
            this.btnAddGMC.UseVisualStyleBackColor = true;
            this.btnAddGMC.Click += new System.EventHandler(this.btnAddGMC_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 39);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(946, 291);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(965, 734);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.rtbLogTab3);
            this.tabPage3.Controls.Add(this.txtTitleFilter);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.btnClone);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(957, 708);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Clone Product";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(91, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Logs";
            // 
            // rtbLogTab3
            // 
            this.rtbLogTab3.Location = new System.Drawing.Point(91, 143);
            this.rtbLogTab3.Name = "rtbLogTab3";
            this.rtbLogTab3.Size = new System.Drawing.Size(811, 470);
            this.rtbLogTab3.TabIndex = 8;
            this.rtbLogTab3.Text = "";
            // 
            // txtTitleFilter
            // 
            this.txtTitleFilter.Location = new System.Drawing.Point(91, 79);
            this.txtTitleFilter.Name = "txtTitleFilter";
            this.txtTitleFilter.Size = new System.Drawing.Size(219, 20);
            this.txtTitleFilter.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(88, 63);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Search";
            // 
            // btnClone
            // 
            this.btnClone.Location = new System.Drawing.Point(357, 76);
            this.btnClone.Name = "btnClone";
            this.btnClone.Size = new System.Drawing.Size(145, 23);
            this.btnClone.TabIndex = 0;
            this.btnClone.Text = "Clone Customize";
            this.btnClone.UseVisualStyleBackColor = true;
            this.btnClone.Click += new System.EventHandler(this.btnClone_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnBackupShopBase);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.checkedListStore);
            this.tabPage4.Controls.Add(this.btnPublishAll);
            this.tabPage4.Controls.Add(this.btnExportBusinessData);
            this.tabPage4.Controls.Add(this.btnRestoreToShopbaseCSV);
            this.tabPage4.Controls.Add(this.btnExportShopbaseCSV);
            this.tabPage4.Controls.Add(this.txtTitle);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.btnSynchronization);
            this.tabPage4.Controls.Add(this.btnProductCount);
            this.tabPage4.Controls.Add(this.btnDeleteProductError);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.comboBox1);
            this.tabPage4.Controls.Add(this.lblLogLastTimeSuccessRestore);
            this.tabPage4.Controls.Add(this.lblLogRestoreShopify);
            this.tabPage4.Controls.Add(this.lblFolderRestore);
            this.tabPage4.Controls.Add(this.btnRestoreShopify);
            this.tabPage4.Controls.Add(this.btnBackupShopify);
            this.tabPage4.Controls.Add(this.rtbLogTab4);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Controls.Add(this.rtbListRepository);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.txtProductType);
            this.tabPage4.Controls.Add(this.label4);
            this.tabPage4.Controls.Add(this.btnPrepare);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(957, 708);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Product warehouse";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // btnBackupShopBase
            // 
            this.btnBackupShopBase.Location = new System.Drawing.Point(879, 152);
            this.btnBackupShopBase.Name = "btnBackupShopBase";
            this.btnBackupShopBase.Size = new System.Drawing.Size(75, 23);
            this.btnBackupShopBase.TabIndex = 25;
            this.btnBackupShopBase.Text = "Backup SB";
            this.btnBackupShopBase.UseVisualStyleBackColor = true;
            this.btnBackupShopBase.Click += new System.EventHandler(this.btnBackupShopBase_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(512, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(104, 13);
            this.label14.TabIndex = 24;
            this.label14.Text = "Shopify Store Center";
            // 
            // checkedListStore
            // 
            this.checkedListStore.FormattingEnabled = true;
            this.checkedListStore.Location = new System.Drawing.Point(515, 37);
            this.checkedListStore.Name = "checkedListStore";
            this.checkedListStore.Size = new System.Drawing.Size(277, 364);
            this.checkedListStore.TabIndex = 23;
            this.checkedListStore.SelectedIndexChanged += new System.EventHandler(this.checkedListStore_SelectedIndexChanged);
            // 
            // btnPublishAll
            // 
            this.btnPublishAll.Location = new System.Drawing.Point(842, 473);
            this.btnPublishAll.Name = "btnPublishAll";
            this.btnPublishAll.Size = new System.Drawing.Size(75, 23);
            this.btnPublishAll.TabIndex = 22;
            this.btnPublishAll.Text = "Publish All";
            this.btnPublishAll.UseVisualStyleBackColor = true;
            this.btnPublishAll.Click += new System.EventHandler(this.btnPublishAll_Click);
            // 
            // btnExportBusinessData
            // 
            this.btnExportBusinessData.Location = new System.Drawing.Point(812, 428);
            this.btnExportBusinessData.Name = "btnExportBusinessData";
            this.btnExportBusinessData.Size = new System.Drawing.Size(120, 23);
            this.btnExportBusinessData.TabIndex = 21;
            this.btnExportBusinessData.Text = "Export Business Data Feed";
            this.btnExportBusinessData.UseVisualStyleBackColor = true;
            this.btnExportBusinessData.Click += new System.EventHandler(this.btnExportBusinessData_Click);
            // 
            // btnRestoreToShopbaseCSV
            // 
            this.btnRestoreToShopbaseCSV.Location = new System.Drawing.Point(798, 391);
            this.btnRestoreToShopbaseCSV.Name = "btnRestoreToShopbaseCSV";
            this.btnRestoreToShopbaseCSV.Size = new System.Drawing.Size(156, 23);
            this.btnRestoreToShopbaseCSV.TabIndex = 20;
            this.btnRestoreToShopbaseCSV.Text = "Restore To Shopbase CSV";
            this.btnRestoreToShopbaseCSV.UseVisualStyleBackColor = true;
            this.btnRestoreToShopbaseCSV.Click += new System.EventHandler(this.btnRestoreToShopbaseCSV_Click);
            // 
            // btnExportShopbaseCSV
            // 
            this.btnExportShopbaseCSV.Location = new System.Drawing.Point(798, 351);
            this.btnExportShopbaseCSV.Name = "btnExportShopbaseCSV";
            this.btnExportShopbaseCSV.Size = new System.Drawing.Size(156, 23);
            this.btnExportShopbaseCSV.TabIndex = 19;
            this.btnExportShopbaseCSV.Text = "Export To Shopbase CSV";
            this.btnExportShopbaseCSV.UseVisualStyleBackColor = true;
            this.btnExportShopbaseCSV.Click += new System.EventHandler(this.btnExportShopbaseCSV_Click);
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(35, 37);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(455, 20);
            this.txtTitle.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(32, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Title";
            // 
            // btnSynchronization
            // 
            this.btnSynchronization.Location = new System.Drawing.Point(812, 311);
            this.btnSynchronization.Name = "btnSynchronization";
            this.btnSynchronization.Size = new System.Drawing.Size(120, 23);
            this.btnSynchronization.TabIndex = 16;
            this.btnSynchronization.Text = "Synchronization";
            this.btnSynchronization.UseVisualStyleBackColor = true;
            this.btnSynchronization.Click += new System.EventHandler(this.btnSynchronization_Click);
            // 
            // btnProductCount
            // 
            this.btnProductCount.Location = new System.Drawing.Point(812, 273);
            this.btnProductCount.Name = "btnProductCount";
            this.btnProductCount.Size = new System.Drawing.Size(120, 23);
            this.btnProductCount.TabIndex = 15;
            this.btnProductCount.Text = "Product Count";
            this.btnProductCount.UseVisualStyleBackColor = true;
            this.btnProductCount.Click += new System.EventHandler(this.btnProductCount_Click);
            // 
            // btnDeleteProductError
            // 
            this.btnDeleteProductError.Location = new System.Drawing.Point(812, 232);
            this.btnDeleteProductError.Name = "btnDeleteProductError";
            this.btnDeleteProductError.Size = new System.Drawing.Size(120, 23);
            this.btnDeleteProductError.TabIndex = 14;
            this.btnDeleteProductError.Text = "Delete product error";
            this.btnDeleteProductError.UseVisualStyleBackColor = true;
            this.btnDeleteProductError.Click += new System.EventHandler(this.btnDeleteProductError_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(304, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Restore to Store";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(304, 87);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(186, 21);
            this.comboBox1.TabIndex = 12;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lblLogLastTimeSuccessRestore
            // 
            this.lblLogLastTimeSuccessRestore.AutoSize = true;
            this.lblLogLastTimeSuccessRestore.Location = new System.Drawing.Point(39, 668);
            this.lblLogLastTimeSuccessRestore.Name = "lblLogLastTimeSuccessRestore";
            this.lblLogLastTimeSuccessRestore.Size = new System.Drawing.Size(83, 13);
            this.lblLogLastTimeSuccessRestore.TabIndex = 11;
            this.lblLogLastTimeSuccessRestore.Text = "Latest Success:";
            // 
            // lblLogRestoreShopify
            // 
            this.lblLogRestoreShopify.AutoSize = true;
            this.lblLogRestoreShopify.Location = new System.Drawing.Point(239, 668);
            this.lblLogRestoreShopify.Name = "lblLogRestoreShopify";
            this.lblLogRestoreShopify.Size = new System.Drawing.Size(65, 13);
            this.lblLogRestoreShopify.TabIndex = 10;
            this.lblLogRestoreShopify.Text = "Log Restore";
            // 
            // lblFolderRestore
            // 
            this.lblFolderRestore.AutoSize = true;
            this.lblFolderRestore.Location = new System.Drawing.Point(304, 122);
            this.lblFolderRestore.Name = "lblFolderRestore";
            this.lblFolderRestore.Size = new System.Drawing.Size(0, 13);
            this.lblFolderRestore.TabIndex = 9;
            // 
            // btnRestoreShopify
            // 
            this.btnRestoreShopify.Location = new System.Drawing.Point(834, 192);
            this.btnRestoreShopify.Name = "btnRestoreShopify";
            this.btnRestoreShopify.Size = new System.Drawing.Size(75, 23);
            this.btnRestoreShopify.TabIndex = 8;
            this.btnRestoreShopify.Text = "Restore";
            this.btnRestoreShopify.UseVisualStyleBackColor = true;
            this.btnRestoreShopify.Click += new System.EventHandler(this.btnRestoreShopify_Click);
            // 
            // btnBackupShopify
            // 
            this.btnBackupShopify.Location = new System.Drawing.Point(798, 152);
            this.btnBackupShopify.Name = "btnBackupShopify";
            this.btnBackupShopify.Size = new System.Drawing.Size(75, 23);
            this.btnBackupShopify.TabIndex = 7;
            this.btnBackupShopify.Text = "Backup SPF";
            this.btnBackupShopify.UseVisualStyleBackColor = true;
            this.btnBackupShopify.Click += new System.EventHandler(this.btnBackupShopify_Click);
            // 
            // rtbLogTab4
            // 
            this.rtbLogTab4.Location = new System.Drawing.Point(34, 430);
            this.rtbLogTab4.Name = "rtbLogTab4";
            this.rtbLogTab4.Size = new System.Drawing.Size(758, 235);
            this.rtbLogTab4.TabIndex = 6;
            this.rtbLogTab4.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 414);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Logs";
            // 
            // rtbListRepository
            // 
            this.rtbListRepository.Location = new System.Drawing.Point(31, 142);
            this.rtbListRepository.Name = "rtbListRepository";
            this.rtbListRepository.Size = new System.Drawing.Size(459, 261);
            this.rtbListRepository.TabIndex = 4;
            this.rtbListRepository.Text = "";
            this.rtbListRepository.TextChanged += new System.EventHandler(this.rtbListRepository_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(32, 122);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "List Repository [Url Api Pass]";
            // 
            // txtProductType
            // 
            this.txtProductType.Location = new System.Drawing.Point(35, 88);
            this.txtProductType.Name = "txtProductType";
            this.txtProductType.Size = new System.Drawing.Size(244, 20);
            this.txtProductType.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "List of product types separated by commas";
            // 
            // btnPrepare
            // 
            this.btnPrepare.Location = new System.Drawing.Point(834, 112);
            this.btnPrepare.Name = "btnPrepare";
            this.btnPrepare.Size = new System.Drawing.Size(75, 23);
            this.btnPrepare.TabIndex = 0;
            this.btnPrepare.Text = "Prepare Repository";
            this.btnPrepare.UseVisualStyleBackColor = true;
            this.btnPrepare.Click += new System.EventHandler(this.btnPrepare_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.btnExportTab4);
            this.tabPage5.Controls.Add(this.btnUpdateInTab5);
            this.tabPage5.Controls.Add(this.label10);
            this.tabPage5.Controls.Add(this.comboBox2);
            this.tabPage5.Controls.Add(this.label11);
            this.tabPage5.Controls.Add(this.rtbTab5);
            this.tabPage5.Controls.Add(this.label12);
            this.tabPage5.Controls.Add(this.rtbListTitle);
            this.tabPage5.Controls.Add(this.label13);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(957, 708);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Filter Products By List Title";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // btnExportTab4
            // 
            this.btnExportTab4.Location = new System.Drawing.Point(788, 114);
            this.btnExportTab4.Name = "btnExportTab4";
            this.btnExportTab4.Size = new System.Drawing.Size(166, 23);
            this.btnExportTab4.TabIndex = 24;
            this.btnExportTab4.Text = "Export Product By Title";
            this.btnExportTab4.UseVisualStyleBackColor = true;
            this.btnExportTab4.Click += new System.EventHandler(this.btnExportTab4_Click);
            // 
            // btnUpdateInTab5
            // 
            this.btnUpdateInTab5.Location = new System.Drawing.Point(809, 85);
            this.btnUpdateInTab5.Name = "btnUpdateInTab5";
            this.btnUpdateInTab5.Size = new System.Drawing.Size(120, 23);
            this.btnUpdateInTab5.TabIndex = 23;
            this.btnUpdateInTab5.Text = "Sold Out By List Title";
            this.btnUpdateInTab5.UseVisualStyleBackColor = true;
            this.btnUpdateInTab5.Click += new System.EventHandler(this.btnDeleteInTab4_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 14);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(84, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Restore to Store";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(23, 30);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(186, 21);
            this.comboBox2.TabIndex = 21;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(296, 65);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 20;
            // 
            // rtbTab5
            // 
            this.rtbTab5.Location = new System.Drawing.Point(26, 373);
            this.rtbTab5.Name = "rtbTab5";
            this.rtbTab5.Size = new System.Drawing.Size(758, 235);
            this.rtbTab5.TabIndex = 19;
            this.rtbTab5.Text = "";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 357);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "Logs";
            // 
            // rtbListTitle
            // 
            this.rtbListTitle.Location = new System.Drawing.Point(23, 85);
            this.rtbListTitle.Name = "rtbListTitle";
            this.rtbListTitle.Size = new System.Drawing.Size(761, 261);
            this.rtbListTitle.TabIndex = 17;
            this.rtbListTitle.Text = "";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(24, 65);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "List Title";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.btnImportADS);
            this.tabPage6.Controls.Add(this.rtbADS);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(957, 708);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "ADS Import";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // btnImportADS
            // 
            this.btnImportADS.Location = new System.Drawing.Point(843, 464);
            this.btnImportADS.Name = "btnImportADS";
            this.btnImportADS.Size = new System.Drawing.Size(75, 23);
            this.btnImportADS.TabIndex = 1;
            this.btnImportADS.Text = "Import";
            this.btnImportADS.UseVisualStyleBackColor = true;
            this.btnImportADS.Click += new System.EventHandler(this.btnImportADS_Click);
            // 
            // rtbADS
            // 
            this.rtbADS.Location = new System.Drawing.Point(3, 191);
            this.rtbADS.Name = "rtbADS";
            this.rtbADS.Size = new System.Drawing.Size(902, 181);
            this.rtbADS.TabIndex = 0;
            this.rtbADS.Text = "";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 758);
            this.Controls.Add(this.tabControl1);
            this.Name = "Main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.accountBindingSource)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource accountBindingSource;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnAddStore;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.RichTextBox rtbResponse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddGMC;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.CheckBox checkBoxUpdateProducts;
        private System.Windows.Forms.CheckBox checkBoxUpdatePage;
        private System.Windows.Forms.CheckBox checkBoxUpdateCollections;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnClone;
        private System.Windows.Forms.TextBox txtTitleFilter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox rtbLogTab3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button btnUpdateInTab5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.RichTextBox rtbTab5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RichTextBox rtbListTitle;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnExportTab4;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button btnImportADS;
        private System.Windows.Forms.RichTextBox rtbADS;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.CheckedListBox checkedListStore;
        private System.Windows.Forms.Button btnPublishAll;
        private System.Windows.Forms.Button btnExportBusinessData;
        private System.Windows.Forms.Button btnRestoreToShopbaseCSV;
        private System.Windows.Forms.Button btnExportShopbaseCSV;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSynchronization;
        private System.Windows.Forms.Button btnProductCount;
        private System.Windows.Forms.Button btnDeleteProductError;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lblLogLastTimeSuccessRestore;
        private System.Windows.Forms.Label lblLogRestoreShopify;
        private System.Windows.Forms.Label lblFolderRestore;
        private System.Windows.Forms.Button btnRestoreShopify;
        private System.Windows.Forms.Button btnBackupShopify;
        private System.Windows.Forms.RichTextBox rtbLogTab4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox rtbListRepository;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtProductType;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPrepare;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnBackupShopBase;
    }
}

