﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolCheckGMC
{
    public partial class AddNewStore : Form
    {
        public AddNewStore()
        {
            InitializeComponent();
        }
        private void btnAddStore_Click(object sender, EventArgs e)
        {
            try
            {
                using (MerchantCenterEntities db = new MerchantCenterEntities()) {
                    var idMerchant = !string.IsNullOrEmpty(txtGMCID.Text) ? int.Parse(txtGMCID.Text) : new Nullable<int>();
                    var newStore = new Shopbase_Store
                    {
                        AccountGMCId = idMerchant,
                        Address = txtAddress.Text,
                        PhoneNumber = txtPhoneNumber.Text,
                        EmailSupport = txtEmailSupport.Text,
                        StoreName = txtStoreName.Text,
                        TypeProject = int.Parse(txtTypeProject.Text),
                        AdminUrl = txtAdminUrl.Text,
                        ApiKey = txtApiKey.Text,
                        Password = txtPassword.Text
                    };
                    db.Shopbase_Stores.Add(newStore);
                    db.SaveChanges();
                    MessageBox.Show("Add store successfully!", "Complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error add new store!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddNewStore_Load(object sender, EventArgs e)
        {

        }
        private void txtGMCID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
        private void txtTypeProoject_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
