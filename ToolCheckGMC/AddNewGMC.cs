﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToolCheckGMC
{
    public partial class AddNewGMC : Form
    {
        public AddNewGMC()
        {
            InitializeComponent();
        }

        private void AddNewGMC_Load(object sender, EventArgs e)
        {

        }
        private void txtGMCID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                using (MerchantCenterEntities db = new MerchantCenterEntities())
                {
                    if (string.IsNullOrEmpty(txtStoreName.Text))
                        throw new Exception("Store name must not be blank!");
                    if (string.IsNullOrEmpty(txtMerchantId.Text))
                        throw new Exception("MerchantId must not be blank!");
                    if (string.IsNullOrEmpty(rtbJson.Text))
                        throw new Exception("Json token must not be blank!");
                    var idMerchant = int.Parse(txtMerchantId.Text);
                    if(db.Accounts.Where(p=>p.MerchantId == idMerchant).Any())
                    {
                        throw new Exception("Account already exists");
                    }
                    var newAccount = new Account
                    {
                        MerchantId = idMerchant,
                        Note = txtNote.Text,
                        StoreName = txtStoreName.Text,
                        Json = rtbJson.Text,
                        CreationTime = DateTime.Now,
                        IsActive = true,
                        TotalActive = 0,
                        TotalDisapproved = 0,
                        TotalExpiring = 0,
                        TotalPending = 0,
                    };
                    db.Accounts.Add(newAccount);
                    db.SaveChanges();
                    Close();
                    MessageBox.Show("Add account successfully!", "Complete!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error add new store!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
