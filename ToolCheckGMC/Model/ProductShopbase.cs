﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolCheckGMC.Model
{
    public partial class ProductShopbase
    {
        [JsonProperty("body_html")]
        public string BodyHtml { get; set; }

        [JsonProperty("can_preview")]
        public bool CanPreview { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("custom_options")]
        public List<CustomOption> CustomOptions { get; set; }

        [JsonProperty("display_options")]
        public DisplayOptions DisplayOptions { get; set; }

        [JsonProperty("fulfillment_services")]
        public List<FulfillmentService> FulfillmentServices { get; set; }

        [JsonProperty("handle")]
        public string Handle { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("image")]
        public ImageShopbase Image { get; set; }

        [JsonProperty("images")]
        public List<ImageShopbase> Images { get; set; }

        [JsonProperty("metafields_global_description_tag")]
        public string MetafieldsGlobalDescriptionTag { get; set; }

        [JsonProperty("metafields_global_title_tag")]
        public string MetafieldsGlobalTitleTag { get; set; }

        [JsonProperty("options")]
        public List<OptionShopbase> Options { get; set; }

        [JsonProperty("product_availability")]
        //1.Available in all channels
        //2.Hide from the sitemap.txt
        //3.Hide from online store listing pages (homepage, collection pages, search page,...)
        //4.Hide from all channels
        public int ProductAvailability { get; set; }

        [JsonProperty("product_type")]
        public string ProductType { get; set; }

        [JsonProperty("published")]
        public bool Published { get; set; }

        [JsonProperty("published_at")]
        public string PublishedAt { get; set; }

        [JsonProperty("tags")]
        public string Tags { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("variants")]
        public List<VariantShopbase> Variants { get; set; }

        [JsonProperty("vendor")]
        public string Vendor { get; set; }
    }

    public partial class CustomOption
    {
        [JsonProperty("active")]
        public bool Active { get; set; }

        [JsonProperty("default_value")]
        public string DefaultValue { get; set; }

        [JsonProperty("default_value_index")]
        public long? DefaultValueIndex { get; set; }

        [JsonProperty("font_family")]
        public string FontFamily { get; set; }

        [JsonProperty("font_path")]
        public string FontPath { get; set; }

        [JsonProperty("help_text")]
        public string HelpText { get; set; }

        [JsonProperty("id")]
        public long? Id { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("layer_name")]
        public string LayerName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("p_o_d_data")]
        public PODData PODData { get; set; }

        [JsonProperty("placeholder")]
        public string Placeholder { get; set; }

        [JsonProperty("rules")]
        public List<Rule> Rules { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("validations")]
        public Validations Validations { get; set; }

        [JsonProperty("values")]
        public List<CustomOptionValue> Values { get; set; }
    }

    public partial class PODData
    {
        [JsonProperty("clip_art_id")]
        public long ClipArtId { get; set; }

        [JsonProperty("clip_art_type")]
        public string ClipArtType { get; set; }

        [JsonProperty("layer_data")]
        public string LayerData { get; set; }

        [JsonProperty("layers")]
        public List<Layer> Layers { get; set; }

        [JsonProperty("show_type")]
        public string ShowType { get; set; }
    }

    public partial class Layer
    {
        [JsonProperty("base_product_id")]
        public long BaseProductId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("option_id")]
        public long OptionId { get; set; }

        [JsonProperty("unique")]
        public long Unique { get; set; }
    }

    public partial class Rule
    {
        [JsonProperty("conditions")]
        public List<Condition> Conditions { get; set; }

        [JsonProperty("show_option_ids")]
        public List<long> ShowOptionIds { get; set; }
    }

    public partial class Condition
    {
        [JsonProperty("relation")]
        public string Relation { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class Validations
    {
        [JsonProperty("allowed_characters")]
        public List<string> AllowedCharacters { get; set; }

        [JsonProperty("max_length")]
        public long MaxLength { get; set; }

        [JsonProperty("min_size")]
        public long MinSize { get; set; }

        [JsonProperty("required")]
        public bool ValidationsRequired { get; set; }
    }

    public partial class CustomOptionValue
    {
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class DisplayOptions
    {
        [JsonProperty("group_options_by")]
        public int GroupOptionsBy { get; set; }
    }

    public partial class FulfillmentService
    {
        [JsonProperty("can_remove_mapping")]
        public bool CanRemoveMapping { get; set; }

        [JsonProperty("is_mapped")]
        public bool IsMapped { get; set; }

        [JsonProperty("service_name")]
        public string ServiceName { get; set; }
    }

    public partial class ImageShopbase
    {
        [JsonProperty("alt_text")]
        public string AltText { get; set; }

        [JsonProperty("height")]
        public int? Height { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("product_id")]
        public long ProductId { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("src")]
        public string Src { get; set; }

        [JsonProperty("attachment")]
        public string Attachment { get; set; }

        [JsonProperty("filename")]
        public string Filename { get; set; }


        [JsonProperty("variant_ids")]
        public List<long> VariantIds { get; set; }

        [JsonProperty("width")]
        public int? Width { get; set; }
    }

    public partial class OptionShopbase
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("product_id")]
        public long ProductId { get; set; }

        [JsonProperty("values")]
        public List<string> Values { get; set; }
    }

    public partial class VariantShopbase
    {
        [JsonProperty("barcode")]
        public string Barcode { get; set; }

        [JsonProperty("compare_at_price")]
        public decimal? CompareAtPrice { get; set; }

        [JsonProperty("cost_per_item")]
        public decimal? CostPerItem { get; set; }

        [JsonProperty("fulfillment_service")]
        public string FulfillmentService { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("image_id")]
        public long ImageId { get; set; }

        [JsonProperty("inventory_management")]
        public string InventoryManagement { get; set; }

        [JsonProperty("inventory_policy")]
        public string InventoryPolicy { get; set; }

        [JsonProperty("inventory_quantity")]
        public int InventoryQuantity { get; set; }

        [JsonProperty("is_default")]
        public bool IsDefault { get; set; }

        [JsonProperty("metafields")]
        public List<Metafield> Metafields { get; set; }

        [JsonProperty("option1")]
        public string Option1 { get; set; }

        [JsonProperty("option2")]
        public string Option2 { get; set; }

        [JsonProperty("option3")]
        public string Option3 { get; set; }

        [JsonProperty("position")]
        public long Position { get; set; }

        [JsonProperty("price")]
        public decimal Price { get; set; }

        [JsonProperty("product_id")]
        public long ProductId { get; set; }

        [JsonProperty("requires_shipping")]
        public bool RequiresShipping { get; set; }

        [JsonProperty("sku")]
        public string Sku { get; set; }

        [JsonProperty("taxable")]
        public bool Taxable { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("weight")]
        public string Weight { get; set; }

        [JsonProperty("weight_unit")]
        public string WeightUnit { get; set; }
    }

    public partial class Metafield
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("namespace")]
        public string Namespace { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("value_type")]
        public string ValueType { get; set; }
    }
}
