﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ToolCheckGMC.Model;

namespace ToolCheckGMC.ImportServices
{
    public static class ProductStorage
    {
        #region Database Access
        public static ShopifyCenterStore ShopifyCenterStore(int id)
        {
            using (bonzaroEntities db = new bonzaroEntities())
            {
                return db.ShopifyCenterStores.Find(id);
            }
        }
        public static void UpdateImportProduct(ShopifyImportProduct model)
        {
            using (bonzaroEntities db = new bonzaroEntities())
            {
                var current = db.ShopifyImportProducts.Find(model.Id);
                if (current == null)
                    throw new System.Exception("Import product could not be loaded");
                current.IsCustomized = model.IsCustomized;
                current.Title = model.Title;
                current.Url = model.Url;
                current.Status = model.Status;
                current.ProductId = model.ProductId;
                current.DefaultPicture = model.DefaultPicture;
                current.ImportedOnUtc = model.ImportedOnUtc;
                bool saved = false;
                int retry = 0;
                while (!saved)
                {
                    try
                    {
                        db.SaveChanges();
                        saved = true;
                    }
                    catch (Exception ex)
                    {
                        if (retry == 3)
                            throw ex;
                        retry++;
                        //sleep 5s and continue
                        Thread.Sleep(5000);
                    }
                }
            }
        }
        public static void CreatePreImportProduct(PreImportProduct input)
        {
            using (bonzaroEntities db = new bonzaroEntities())
            {
                db.PreImportProducts.Add(input);
                db.SaveChanges();
            }
        }
        public static PreImportProduct PreImportProduct(int id)
        {
            using (bonzaroEntities db = new bonzaroEntities())
            {
                return db.PreImportProducts.Include("ShopifyImportProduct").Include("ImportProductImages").Include("ImportProductOptions").Include("ImportProductVariants").FirstOrDefault(p => p.Id == id);
            }
        }
        public static PreImportProduct ImportProductData(int importProductId)
        {
            using (bonzaroEntities db = new bonzaroEntities())
            {
                return db.PreImportProducts.Where(p => !p.Deleted).Include("ShopifyImportProduct").Include("ImportProductImages").Include("ImportProductOptions").Include("ImportProductVariants").FirstOrDefault(p => p.ImportProductId == importProductId);
            }
        }
        public static void SyncPreImportProduct(int importProductId)
        {
            using (bonzaroEntities db = new bonzaroEntities())
            {
                var importProduct = db.ShopifyImportProducts.FirstOrDefault(p => p.Id == importProductId);
                if (importProduct == null)
                    throw new Exception("Invalid import product id");
                var preImportProduct = db.PreImportProducts.Where(p => !p.Deleted).FirstOrDefault(p => p.ImportProductId == importProductId);
                if (preImportProduct == null)
                    throw new Exception("No pre import product was found");
                preImportProduct.Title = importProduct.Title;
                string tags = importProduct.IsTradeMark.HasValue ? importProduct.IsTradeMark.Value ? Common.ShopifyTagTM : "" : "";
                if (!string.IsNullOrEmpty(tags))
                    preImportProduct.Tags = string.IsNullOrEmpty(preImportProduct.Tags) ? tags : preImportProduct.Tags.Contains(tags) ? preImportProduct.Tags : $"{preImportProduct.Tags},{tags}";
                preImportProduct.UpdatedOnUtc = DateTime.UtcNow;
                db.SaveChanges();
            }
        }
        #endregion

        #region PreImporter
        //public static async Task<PreImportProduct> ShopifyPreImport(ShopifyImportProduct importProduct, string tags = "")
        //{
        //    //remove query string from product url to get json source
        //    var uri = new Uri(importProduct.Url);
        //    string originUrl = $"{uri.Scheme}://{uri.Host}{uri.AbsolutePath}";
        //    if (originUrl.EndsWith("/"))
        //        originUrl = originUrl.TrimEnd('/');
        //    //get shopify product info
        //    var productUri = new Uri($"{originUrl}.json");
        //    var shopifyProduct = await ShopifyStorefront.ExecuteRequestAsync<ShopifySharp.Product>(productUri, HttpMethod.Get, rootElement: "product");
        //    //retry to get product
        //    if (shopifyProduct == null)
        //        shopifyProduct = await ShopifyStorefront.ExecuteRequestAsync<ShopifySharp.Product>(productUri, HttpMethod.Get, rootElement: "product");
        //    if (shopifyProduct == null)
        //        throw new Exception($"No product could be loaded with url {originUrl}");

        //    if (shopifyProduct == null)
        //        throw new Exception("shopify product is null");
        //    using (bonzaroEntities db = new bonzaroEntities())
        //    {
        //        using (DbContextTransaction transaction = db.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                var _importProduct = db.ShopifyImportProducts.FirstOrDefault(p => p.Id == importProduct.Id);
        //                if (_importProduct == null)
        //                    throw new Exception("No import product was found");
        //                PreImportProduct preImportProduct = null;
        //                var preImportProducts = db.PreImportProducts.Where(p => !p.Deleted && p.ImportProductId == importProduct.Id).ToList();
        //                if (preImportProducts.Any())
        //                    preImportProduct = preImportProducts.Count > 1 ? preImportProduct = preImportProducts.FirstOrDefault(p => p.Handle == shopifyProduct.Handle) : preImportProducts.FirstOrDefault();

        //                //basic info
        //                if (preImportProduct == null)
        //                {
        //                    preImportProduct = new PreImportProduct();
        //                    preImportProduct.Title = shopifyProduct.Title;
        //                    preImportProduct.ProductType = shopifyProduct.ProductType;
        //                    preImportProduct.BodyHtml = shopifyProduct.BodyHtml;
        //                    preImportProduct.Deleted = false;
        //                    preImportProduct.Tags = string.IsNullOrEmpty(tags) ? shopifyProduct.Tags : tags;
        //                    preImportProduct.Handle = shopifyProduct.Handle;
        //                    preImportProduct.CreatedOnUtc = DateTime.UtcNow;
        //                    preImportProduct.ImportProductId = _importProduct.Id;
        //                    db.PreImportProducts.Add(preImportProduct);
        //                    db.SaveChanges();
        //                }
        //                int savedImages = 0;
        //                //iamges
        //                foreach (var productImage in shopifyProduct.Images)
        //                {
        //                    try
        //                    {
        //                        var imageStorage = ImportImageStorage.SaveToStorage(src: productImage.Src, entities: db);
        //                        if (string.IsNullOrEmpty(_importProduct.DefaultPicture))
        //                        {
        //                            var cloudImageUrl = ImportImageStorage.SaveToCloud(imageStorage, shopifyProduct.Title, tags);
        //                            _importProduct.DefaultPicture = cloudImageUrl;
        //                            db.SaveChanges();
        //                        }

        //                        var importProductImage = new ImportProductImage
        //                        {
        //                            ImageId = imageStorage.Id,
        //                            ImportProductId = preImportProduct.Id,
        //                            CreatedOnUtc = DateTime.UtcNow
        //                        };
        //                        db.ImportProductImages.Add(importProductImage);
        //                        db.SaveChanges();
        //                        savedImages++;
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                    }
        //                }
        //                if (savedImages == 0)
        //                    throw new Exception("No image was saved");
        //                //options
        //                foreach (var option in shopifyProduct.Options)
        //                {
        //                    var importOption = new ImportProductOption();
        //                    importOption.Name = option.Name;
        //                    importOption.Position = option.Position ?? 0;
        //                    importOption.Values = option.Values.Any() ? string.Join(",", option.Values) : null;
        //                    importOption.ImportProductId = preImportProduct.Id;
        //                    db.ImportProductOptions.Add(importOption);
        //                    db.SaveChanges();
        //                }
        //                //variant
        //                foreach (var variant in shopifyProduct.Variants)
        //                {
        //                    var importVariant = new ImportProductVariant();
        //                    importVariant.ImportProductId = preImportProduct.Id;
        //                    importVariant.Title = variant.Title;
        //                    importVariant.Option1 = variant.Option1;
        //                    importVariant.Option2 = variant.Option2;
        //                    importVariant.Option3 = variant.Option3;
        //                    importVariant.Position = variant.Position ?? 0;
        //                    importVariant.Sku = variant.SKU;
        //                    var variantIamge = shopifyProduct.Images.FirstOrDefault(p => p.Id == variant.ImageId);
        //                    if (variantIamge != null)
        //                    {
        //                        var imageStorage = ImportImageStorage.SaveToStorage(src: variantIamge.Src, entities: db);
        //                        importVariant.ImageId = imageStorage.Id;
        //                    }
        //                    db.ImportProductVariants.Add(importVariant);
        //                    db.SaveChanges();
        //                }
        //                //mark import product as imported
        //                _importProduct.Title = preImportProduct.Title;
        //                _importProduct.Status = (int)ShopifyImportStatus.Imported;
        //                _importProduct.ImportedOnUtc = DateTime.UtcNow;
        //                _importProduct.Source = "shopify";
        //                db.SaveChanges();
        //                transaction.Commit();
        //                return preImportProduct;
        //            }
        //            catch (Exception ex)
        //            {
        //                transaction.Rollback();
        //                throw ex;

        //            }
        //        }
        //    }
        //}

        //public static async Task<PreImportProduct> ShopbasePreImport(ShopifyImportProduct importProduct, string tags = "")
        //{
        //    try
        //    {
        //        //remove query string from product url to get json source
        //        var uri = new Uri(importProduct.Url);
        //        string originUrl = $"{uri.Scheme}://{uri.Host}{uri.AbsolutePath}";
        //        string storeUrl = $"{uri.Scheme}://{uri.Host}";
        //        string handle = uri.Segments.Last();

        //        var productUri = new Uri($"{storeUrl}/api/catalog/next/product/{handle}.json");
        //        var shopbaseProduct = await ShopifyStorefront.ExecuteRequestAsync<ShopbaseProductDetail>(productUri, System.Net.Http.HttpMethod.Get, rootElement: "result");

        //        //retry to get product
        //        if (shopbaseProduct == null)
        //            shopbaseProduct = await ShopifyStorefront.ExecuteRequestAsync<ShopbaseProductDetail>(productUri, System.Net.Http.HttpMethod.Get, rootElement: "result");
        //        if (shopbaseProduct == null)
        //            throw new Exception($"No product could be loaded with url {originUrl}");

        //        using (bonzaroEntities db = new bonzaroEntities())
        //        {
        //            using (DbContextTransaction transaction = db.Database.BeginTransaction())
        //            {
        //                try
        //                {
        //                    var _importProduct = db.ShopifyImportProducts.FirstOrDefault(p => p.Id == importProduct.Id);
        //                    if (_importProduct == null)
        //                        throw new Exception("No import product was found");

        //                    PreImportProduct preImportProduct = null;
        //                    var preImportProducts = db.PreImportProducts.Where(p => !p.Deleted && p.ImportProductId == importProduct.Id).ToList();
        //                    if (preImportProducts.Any())
        //                        preImportProduct = preImportProducts.Count > 1 ? preImportProduct = preImportProducts.FirstOrDefault(p => p.Handle == shopbaseProduct.Handle) : preImportProducts.FirstOrDefault();

        //                    //basic info
        //                    if (preImportProduct == null)
        //                    {
        //                        preImportProduct = new PreImportProduct();
        //                        preImportProduct.Title = shopbaseProduct.Title;
        //                        preImportProduct.ProductType = shopbaseProduct.ProductType;
        //                        preImportProduct.BodyHtml = shopbaseProduct.Description;
        //                        preImportProduct.Deleted = false;
        //                        preImportProduct.Tags = string.IsNullOrEmpty(tags) ? shopbaseProduct.Tags : tags;
        //                        preImportProduct.Handle = shopbaseProduct.Handle;
        //                        preImportProduct.CreatedOnUtc = DateTime.UtcNow;
        //                        preImportProduct.ImportProductId = _importProduct.Id;
        //                        db.PreImportProducts.Add(preImportProduct);
        //                        db.SaveChanges();
        //                    }

        //                    //iamges
        //                    int savedImages = 0;
        //                    foreach (var productImage in shopbaseProduct.Images)
        //                    {
        //                        try
        //                        {
        //                            var imageStorage = ImportImageStorage.SaveToStorage(src: productImage.Src, entities: db);
        //                            if (string.IsNullOrEmpty(_importProduct.DefaultPicture))
        //                            {
        //                                var cloudImageUrl = ImportImageStorage.SaveToCloud(imageStorage, shopbaseProduct.Title, tags);
        //                                _importProduct.DefaultPicture = cloudImageUrl;
        //                                db.SaveChanges();
        //                            }

        //                            var importProductImage = new ImportProductImage
        //                            {
        //                                ImageId = imageStorage.Id,
        //                                ImportProductId = preImportProduct.Id,
        //                                CreatedOnUtc = DateTime.UtcNow
        //                            };
        //                            db.ImportProductImages.Add(importProductImage);
        //                            db.SaveChanges();
        //                            savedImages++;
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                        }
        //                    }
        //                    if (savedImages == 0)
        //                        throw new Exception("No image was saved");
        //                    //options
        //                    foreach (var option in shopbaseProduct.Options)
        //                    {
        //                        var importOption = new ImportProductOption();
        //                        importOption.Name = option.Name;
        //                        importOption.Position = shopbaseProduct.Options.IndexOf(option);
        //                        importOption.Values = option.Values.Any() ? string.Join(",", option.Values.Where(p => !string.IsNullOrEmpty(p.Name)).Select(p => p.Name).ToList()) : null;
        //                        importOption.ImportProductId = preImportProduct.Id;
        //                        db.ImportProductOptions.Add(importOption);
        //                        db.SaveChanges();
        //                    }
        //                    //variant
        //                    int variantPosition = 0;
        //                    foreach (var variant in shopbaseProduct.Variants)
        //                    {
        //                        var importVariant = new ImportProductVariant();
        //                        importVariant.ImportProductId = preImportProduct.Id;
        //                        importVariant.Title = variant.Title;
        //                        foreach (var option in shopbaseProduct.Options)
        //                        {
        //                            if (!string.IsNullOrEmpty(importVariant.Option1) && !string.IsNullOrEmpty(importVariant.Option2) && !string.IsNullOrEmpty(importVariant.Option3))
        //                                break;

        //                            foreach (var optionValue in option.Values)
        //                            {
        //                                if (!string.IsNullOrEmpty(importVariant.Option1) && !string.IsNullOrEmpty(importVariant.Option2) && !string.IsNullOrEmpty(importVariant.Option3))
        //                                    break;

        //                                if (variant.Option1 == optionValue.Id)
        //                                    importVariant.Option1 = optionValue.Name;
        //                                if (variant.Option2 == optionValue.Id)
        //                                    importVariant.Option2 = optionValue.Name;
        //                                if (variant.Option3 == optionValue.Id)
        //                                    importVariant.Option3 = optionValue.Name;
        //                            }
        //                        }
        //                        importVariant.Position = variantPosition;
        //                        importVariant.Sku = variant.Sku;
        //                        var variantIamge = shopbaseProduct.Images.FirstOrDefault(p => p.Id == variant.ImageId);
        //                        if (variantIamge != null)
        //                        {
        //                            var imageStorage = ImportImageStorage.SaveToStorage(src: variantIamge.Src, entities: db);
        //                            importVariant.ImageId = imageStorage.Id;
        //                        }
        //                        db.ImportProductVariants.Add(importVariant);
        //                        db.SaveChanges();
        //                        variantPosition++;
        //                    }
        //                    //mark import product as imported
        //                    _importProduct.Title = preImportProduct.Title;
        //                    _importProduct.Status = (int)ShopifyImportStatus.Imported;
        //                    _importProduct.ImportedOnUtc = DateTime.UtcNow;
        //                    db.SaveChanges();
        //                    transaction.Commit();
        //                    return preImportProduct;
        //                }
        //                catch (Exception ex)
        //                {
        //                    transaction.Rollback();
        //                    throw ex;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        //public static async Task<PreImportProduct> FanaticsPreImport(ShopifyImportProduct importProduct, string tags = "")
        //{
        //    try
        //    {
        //        //remove query string from product url to get json source
        //        var uri = new Uri(importProduct.Url);
        //        string originUrl = $"{uri.Scheme}://{uri.Host}{uri.AbsolutePath}";
        //        string storeUrl = $"{uri.Scheme}://{uri.Host}";
        //        string handle = uri.Segments.Last();

        //        var productUri = new Uri($"{storeUrl}/api/catalog/next/product/{handle}.json");
        //        var shopbaseProduct = await ShopifyStorefront.ExecuteRequestAsync<ShopbaseProductDetail>(productUri, System.Net.Http.HttpMethod.Get, rootElement: "result");

        //        //retry to get product
        //        if (shopbaseProduct == null)
        //            shopbaseProduct = await ShopifyStorefront.ExecuteRequestAsync<ShopbaseProductDetail>(productUri, System.Net.Http.HttpMethod.Get, rootElement: "result");
        //        if (shopbaseProduct == null)
        //            throw new Exception($"No product could be loaded with url {originUrl}");

        //        using (bonzaroEntities db = new bonzaroEntities())
        //        {
        //            using (DbContextTransaction transaction = db.Database.BeginTransaction())
        //            {
        //                try
        //                {
        //                    var _importProduct = db.ShopifyImportProducts.FirstOrDefault(p => p.Id == importProduct.Id);
        //                    if (_importProduct == null)
        //                        throw new Exception("No import product was found");

        //                    PreImportProduct preImportProduct = null;
        //                    var preImportProducts = db.PreImportProducts.Where(p => !p.Deleted && p.ImportProductId == importProduct.Id).ToList();
        //                    if (preImportProducts.Any())
        //                        preImportProduct = preImportProducts.Count > 1 ? preImportProduct = preImportProducts.FirstOrDefault(p => p.Handle == shopbaseProduct.Handle) : preImportProducts.FirstOrDefault();

        //                    //basic info
        //                    if (preImportProduct == null)
        //                    {
        //                        preImportProduct = new PreImportProduct();
        //                        preImportProduct.Title = shopbaseProduct.Title;
        //                        preImportProduct.ProductType = shopbaseProduct.ProductType;
        //                        preImportProduct.BodyHtml = shopbaseProduct.Description;
        //                        preImportProduct.Deleted = false;
        //                        preImportProduct.Tags = string.IsNullOrEmpty(tags) ? shopbaseProduct.Tags : tags;
        //                        preImportProduct.Handle = shopbaseProduct.Handle;
        //                        preImportProduct.CreatedOnUtc = DateTime.UtcNow;
        //                        preImportProduct.ImportProductId = _importProduct.Id;
        //                        db.PreImportProducts.Add(preImportProduct);
        //                        db.SaveChanges();
        //                    }

        //                    //iamges
        //                    foreach (var productImage in shopbaseProduct.Images)
        //                    {
        //                        try
        //                        {
        //                            var imageStorage = ImportImageStorage.SaveToStorage(src: productImage.Src, entities: db);
        //                            if (string.IsNullOrEmpty(_importProduct.DefaultPicture))
        //                            {
        //                                var cloudImageUrl = ImportImageStorage.SaveToCloud(imageStorage, shopbaseProduct.Title, tags);
        //                                _importProduct.DefaultPicture = cloudImageUrl;
        //                                db.SaveChanges();
        //                            }

        //                            var importProductImage = new ImportProductImage
        //                            {
        //                                ImageId = imageStorage.Id,
        //                                ImportProductId = preImportProduct.Id,
        //                                CreatedOnUtc = DateTime.UtcNow
        //                            };
        //                            db.ImportProductImages.Add(importProductImage);
        //                            db.SaveChanges();
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                        }
        //                    }

        //                    //options
        //                    foreach (var option in shopbaseProduct.Options)
        //                    {
        //                        var importOption = new ImportProductOption();
        //                        importOption.Name = option.Name;
        //                        importOption.Position = shopbaseProduct.Options.IndexOf(option);
        //                        importOption.Values = option.Values.Any() ? string.Join(",", option.Values.Where(p => !string.IsNullOrEmpty(p.Name)).Select(p => p.Name).ToList()) : null;
        //                        importOption.ImportProductId = preImportProduct.Id;
        //                        db.ImportProductOptions.Add(importOption);
        //                        db.SaveChanges();
        //                    }
        //                    //variant
        //                    int variantPosition = 0;
        //                    foreach (var variant in shopbaseProduct.Variants)
        //                    {
        //                        var importVariant = new ImportProductVariant();
        //                        importVariant.ImportProductId = preImportProduct.Id;
        //                        importVariant.Title = variant.Title;
        //                        foreach (var option in shopbaseProduct.Options)
        //                        {
        //                            if (!string.IsNullOrEmpty(importVariant.Option1) && !string.IsNullOrEmpty(importVariant.Option2) && !string.IsNullOrEmpty(importVariant.Option3))
        //                                break;

        //                            foreach (var optionValue in option.Values)
        //                            {
        //                                if (!string.IsNullOrEmpty(importVariant.Option1) && !string.IsNullOrEmpty(importVariant.Option2) && !string.IsNullOrEmpty(importVariant.Option3))
        //                                    break;

        //                                if (variant.Option1 == optionValue.Id)
        //                                    importVariant.Option1 = optionValue.Name;
        //                                if (variant.Option2 == optionValue.Id)
        //                                    importVariant.Option2 = optionValue.Name;
        //                                if (variant.Option3 == optionValue.Id)
        //                                    importVariant.Option3 = optionValue.Name;
        //                            }
        //                        }
        //                        importVariant.Position = variantPosition;
        //                        importVariant.Sku = variant.Sku;
        //                        var variantIamge = shopbaseProduct.Images.FirstOrDefault(p => p.Id == variant.ImageId);
        //                        if (variantIamge != null)
        //                        {
        //                            var imageStorage = ImportImageStorage.SaveToStorage(src: variantIamge.Src, entities: db);
        //                            importVariant.ImageId = imageStorage.Id;
        //                        }
        //                        db.ImportProductVariants.Add(importVariant);
        //                        db.SaveChanges();
        //                        variantPosition++;
        //                    }
        //                    //mark import product as imported
        //                    _importProduct.Title = preImportProduct.Title;
        //                    _importProduct.Status = (int)ShopifyImportStatus.Imported;
        //                    _importProduct.ImportedOnUtc = DateTime.UtcNow;
        //                    db.SaveChanges();
        //                    transaction.Commit();
        //                    return preImportProduct;
        //                }
        //                catch (Exception ex)
        //                {
        //                    transaction.Rollback();
        //                    throw ex;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}
        //#endregion
        //#region Parser
        //public static ProductShopbase ParsePreImportProduct(PreImportProduct preImportProduct)
        //{
        //    try
        //    {
        //        if (preImportProduct == null)
        //            throw new ArgumentNullException("PreImportProduct");

        //        string importImageStorageFolder = ConfigurationManager.AppSettings["ImportImageStorageFolder"].ToString();
        //        if (string.IsNullOrEmpty(importImageStorageFolder))
        //            throw new Exception("ImportImageStorageFolder not found");

        //        //parse base info
        //        var shopbaseProduct = new ProductShopbase();
        //        shopbaseProduct.Title = preImportProduct.Title;
        //        shopbaseProduct.Handle = string.IsNullOrEmpty(preImportProduct.Handle) ? Common.CreateSlug(preImportProduct.Title) : preImportProduct.Handle;
        //        shopbaseProduct.ProductType = preImportProduct.ProductType;
        //        shopbaseProduct.Published = true;
        //        shopbaseProduct.BodyHtml = preImportProduct.BodyHtml;
        //        //parse images
        //        shopbaseProduct.Images = new List<ImageShopbase>();
        //        foreach (var importImage in preImportProduct.ImportProductImages)
        //        {
        //            try
        //            {
        //                if (!importImage.ImageId.HasValue)
        //                    continue;
        //                var imageStorage = ImportImageStorage.RetrieveImageStorage(importImage.ImageId.Value);
        //                if (string.IsNullOrEmpty(imageStorage.Path))
        //                    continue;
        //                var image = new ImageShopbase();

        //                var imageFile = Path.Combine(importImageStorageFolder, imageStorage.Path);
        //                if (!File.Exists(imageFile))
        //                    throw new Exception($"Image file is not existed");
        //                System.Drawing.Image img = null;
        //                if (!imageFile.EndsWith(".webp"))
        //                    img = System.Drawing.Image.FromFile(imageFile);
        //                else
        //                {
        //                    using (WebPWrapper.WebP webp = new WebPWrapper.WebP())
        //                        img = webp.Load(imageFile);
        //                }

        //                if (img == null)
        //                    throw new Exception("Retrived image failed");
        //                byte[] arr;
        //                using (MemoryStream ms = new MemoryStream())
        //                {
        //                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
        //                    arr = ms.ToArray();
        //                }
        //                image.Attachment = Convert.ToBase64String(arr, 0, arr.Length);
        //                shopbaseProduct.Images.Add(image);
        //            }
        //            catch (Exception ex)
        //            {
        //                //do not thing
        //            }
        //        }
        //        //parse options
        //        shopbaseProduct.Options = new List<OptionShopbase>();
        //        foreach (var importOption in preImportProduct.ImportProductOptions)
        //        {
        //            OptionShopbase option = new OptionShopbase();
        //            option.Name = importOption.Name;
        //            option.Position = importOption.Position;
        //            option.ProductId = shopbaseProduct.Id;
        //            option.Values = string.IsNullOrEmpty(importOption.Values) ? new List<string>() : importOption.Values.Split(',').ToList();
        //            shopbaseProduct.Options.Add(option);
        //        }
        //        //parse variant
        //        shopbaseProduct.Variants = new List<VariantShopbase>();
        //        foreach (var importVariant in preImportProduct.ImportProductVariants)
        //        {
        //            VariantShopbase variant = new VariantShopbase();
        //            variant.Title = importVariant.Title;
        //            variant.Sku = importVariant.Sku;
        //            variant.Option1 = importVariant.Option1;
        //            variant.Option2 = importVariant.Option2;
        //            variant.Option3 = importVariant.Option3;
        //            variant.Position = importVariant.Position;
        //            variant.ImageId = importVariant.ImageId ?? 0;
        //            shopbaseProduct.Variants.Add(variant);
        //        }
        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        #endregion
    }
}
