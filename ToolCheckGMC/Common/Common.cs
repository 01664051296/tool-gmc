﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ToolCheckGMC
{
    public static class Common
    {
        public const string ShopifyTagTM = "TradeMarkNe";
        public const string FileLogDreamshipShopify = "LogDreamshipShopify.txt";
        public static string FixTitleBasic(string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;
            source = source.ToLower();
            source = source.Replace("“", " “ ");
            source = source.Replace("”", " ” ");
            source = source.Replace("/", " / ");
            source = source.Replace(" ' ", "'");
            source = source.Replace(",", ", ");
            var words = source.Split(' ');
            words = words.Where(p => !string.IsNullOrEmpty(p)).ToArray();
            for (int i = 0; i < words.Count(); i++)
            {
                char[] letters = words[i].ToCharArray();
                // upper case the first char
                letters[0] = char.ToUpper(letters[0]);
                if ((letters[0].ToString().Contains("\"") || letters[0].ToString().Contains("\"") || letters[0].ToString().Contains("(")) && letters.Count() > 1)
                {
                    letters[1] = char.ToUpper(letters[1]);
                }
                words[i] = new string(letters);
            }
            return String.Join(" ", words.ToArray());
        }
        public static string RemoveLastWord(string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;
            var words = source.Split(' ');
            words = words.Where(p => !string.IsNullOrEmpty(p)).ToArray();
            for (int i = 0; i < words.Count(); i++)
            {
                if (i == words.Count() - 1)
                    words[i] = "";
            }
            return String.Join(" ", words.ToArray());
        }
        public static void LogMaxProductIdDreamshipToShopify(long productId)
        {
            using (StreamWriter newTask = new StreamWriter(Common.FileLogDreamshipShopify, false))
            {
                newTask.WriteLine(productId);
            }
        }
        public static int LoadIndexRestoreShopify(string fileName)
        {
            int result = 0;
            try
            {
                System.IO.StreamReader file =
                new System.IO.StreamReader(fileName);
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    result = Int32.Parse(line);
                }

                file.Close();
            }
            catch
            {
                result = 0;
            }

            return result;
        }

        public static string LoadDreamshipDription(string productType)
        {
            string result = string.Empty;
            try
            {
                System.IO.StreamReader file =
                new System.IO.StreamReader(string.Format("Dreamship/{0}.html", productType));
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    result += line;
                }

                file.Close();
            }
            catch
            {
                result = string.Empty;
            }

            return result;
        }

        public static string LoadShopifyDescription(string productType)
        {
            string result = string.Empty;
            try
            {
                System.IO.StreamReader file =
                new System.IO.StreamReader(string.Format("ShopifyDescription/{0}.html", productType));
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    result += line;
                }

                file.Close();
            }
            catch (Exception ex)
            {
                result = string.Empty;
            }

            return result;
        }

        public static string ParseGroupImportShopify(int groupImport)
        {
            return string.Format("-{0}-", groupImport);
        }
        public static void SaveImportedFileIndex(string fileName, int index)
        {
            using (StreamWriter streamWriter = new StreamWriter(fileName, false))
            {
                streamWriter.WriteLine(index);
            }
        }

        public static int LoadImportedFileIndex(string fileName)
        {
            try
            {
                int result = 0;
                System.IO.StreamReader file = new System.IO.StreamReader(fileName);
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    result = Int32.Parse(line);
                }
                file.Close();
                return result;
            }
            catch
            {
                return 0;
            }

        }

        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        public static string ValidateCSVRecord(string value)
        {
            if (string.IsNullOrEmpty(value))
                return value;
            return value.Replace(",", "");
        }
        public static string GetDatabaseName(string connection)
        {
            try
            {
                foreach (var data in connection.Split(';'))
                {
                    if (string.IsNullOrEmpty(data))
                        continue;
                    if (data.ToLower().StartsWith("initial catalog"))
                    {
                        string database = data.Replace("initial catalog=", "");
                        return database;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return "UNKNOW";
        }

        public static bool IsMatchCondition(string xmlAttribute, int attributeMapping, int attributeValue)
        {
            bool result = false;
            try
            {
                var xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.LoadXml(xmlAttribute);
                var nodeList1 = xmlDoc.SelectNodes(@"//Attributes/ProductAttribute");
                foreach (System.Xml.XmlNode node1 in nodeList1)
                {
                    if (node1.Attributes != null && node1.Attributes["ID"] != null)
                    {
                        string str1 = node1.Attributes["ID"].InnerText.Trim();
                        int id;
                        if (int.TryParse(str1, out id))
                        {
                            if (id == attributeMapping)
                            {
                                var nodeList2 = node1.SelectNodes(@"ProductAttributeValue/Value");
                                foreach (System.Xml.XmlNode node2 in nodeList2)
                                {
                                    string value = node2.InnerText.Trim();
                                    if (int.Parse(value) == attributeValue)
                                    {
                                        result = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }
            return result;
        }
        public static long LoadLogMaxProductIdDreamshipShopify()
        {
            long result = 0;
            try
            {
                System.IO.StreamReader file =
                new System.IO.StreamReader(Common.FileLogDreamshipShopify);
                string line;

                while ((line = file.ReadLine()) != null)
                {
                    result = long.Parse(line);
                }

                file.Close();
            }
            catch
            {
                result = 0;
            }

            return result;
        }
        public static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            return text;
        }
        public enum ShopifyImportStatus
        {
            Pending = 0,
            Imported = 1,
            Verifying = 2,
            Completed = 3,
            Error = 4,
            Ignore = 5,
        }
        public static string CreateSlug(string source)
        {
            var regex = new Regex(@"([^a-z0-9\-]?)");
            var slug = "";

            if (!string.IsNullOrEmpty(source))
            {
                slug = source.Trim().ToLower();
                slug = slug.Replace(' ', '-');
                slug = slug.Replace("---", "-");
                slug = slug.Replace("--", "-");
                if (regex != null)
                    slug = regex.Replace(slug, "");

                if (slug.Length * 2 < source.Length)
                    return "";

                if (slug.Length > 100)
                    slug = slug.Substring(0, 100);
            }
            return slug;
        }
    }
}
