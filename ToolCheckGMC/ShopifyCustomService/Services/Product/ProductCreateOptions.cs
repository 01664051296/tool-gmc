﻿using Newtonsoft.Json;

namespace ShopifyCustomService
{
    public class ProductCreateOptions : Parameterizable
    {
        [JsonProperty("published")]
        public bool? Published { get; set; }
    }
}
