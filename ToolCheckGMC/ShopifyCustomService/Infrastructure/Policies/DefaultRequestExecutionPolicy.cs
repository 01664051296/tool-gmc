﻿using System.Net.Http;
using System.Threading.Tasks;
using ShopifyCustomService.Infrastructure;

namespace ShopifyCustomService
{
    public class DefaultRequestExecutionPolicy : IRequestExecutionPolicy
    {
        public async Task<T> Run<T>(CloneableRequestMessage request, ExecuteRequestAsync<T> executeRequestAsync)
        {
            var fullResult = await executeRequestAsync(request);

            return fullResult.Result;
        }
    }
}
