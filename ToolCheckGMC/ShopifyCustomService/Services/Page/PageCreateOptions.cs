﻿using Newtonsoft.Json;

namespace ShopifyCustomService
{
    public class PageCreateOptions : Parameterizable
    {
        [JsonProperty("published")]
        public bool? Published { get; set; }
    }
}
