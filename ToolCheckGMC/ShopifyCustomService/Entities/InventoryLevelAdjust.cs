using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ShopifyCustomService.Enums;
using ShopifyCustomService.Converters;

namespace ShopifyCustomService
{
    public class InventoryLevelAdjust
    {
        /// <summary>
        /// The unique identifier of the inventory item that the inventory level belongs to.
        /// </summary>
        [JsonProperty("inventory_item_id")]
        public long? InventoryItemId { get; set; }

        /// <summary>
        /// The unique identifier of the location that the inventory level belongs to.
        /// </summary>
        [JsonProperty("location_id")]
        public long? LocationId { get; set; }

        /// <summary>
        /// The quantity adjust of inventory items.
        /// </summary>
        [JsonProperty("available_adjustment")]
        public int? AvailableAdjustment { get; set; }
    }
}